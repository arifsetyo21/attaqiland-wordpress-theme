<?php
/**
 * Template part for displaying a message that posts cannot be found
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package attaqitheme
 */

?>
<?php get_header();?>
   <section class="text-gray-600 body-font">
  <div class="container px-5 py-24 mx-auto">
    <div class="flex flex-col">
      <div class="h-1 bg-gray-200 rounded overflow-hidden">
        <div class="w-24 h-full bg-yellow-500"></div>
      </div>
      <div class="flex flex-wrap sm:flex-row flex-col py-6 mb-12">
        <h1 class="sm:w-2/5 text-blue-550 font-semibold text-4xl mb-2 sm:mb-0">Booklet & Pricelist</h1>
        <p class="sm:w-3/5 leading-relaxed text-base sm:pl-10 pl-0">Booklet dan Pricelist untuk semua kebutuhan informasimu mengenai Attaqi Townhouse.</p>
      </div>
    </div>
    <div class="flex flex-wrap sm:-m-4 -mx-4 -mb-10 -mt-4">
      <div class="p-4 md:w-1/3 sm:mb-0 mb-6">
        <div class="rounded-lg h-64 overflow-hidden">
         <a href="<?php echo get_template_directory_uri(); ?>/assets/img/file-download/Attaqi Leaflet & Jadwal.jpg" download="">
            <img alt="content" class="object-cover object-center h-full w-full" src="<?php echo get_template_directory_uri(); ?>/assets/img/thumbnails/Attaqi Leaflet & Jadwal_tn.jpg">
         </a>
        </div>
        <h2 class="text-xl font-medium title-font text-gray-900 mt-5">Leaflet & Jadwal</h2>
        <!-- <p class="text-base leading-relaxed mt-2">Swag shoivdigoitch literally meditation subway tile tumblr cold-pressed. Gastropub street art beard dreamcatcher neutra, ethical XOXO lumbersexual.</p> -->
        <a class="text-yellow-500 inline-flex items-center mt-3" href="<?php echo get_template_directory_uri(); ?>/assets/img/file-download/Attaqi Leaflet & Jadwal.jpg" download="">Download
          <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" class="w-4 h-4 ml-2" viewBox="0 0 24 24">
            <path d="M5 12h14M12 5l7 7-7 7"></path>
          </svg>
        </a>
      </div>
      <div class="p-4 md:w-1/3 sm:mb-0 mb-6">
        <div class="rounded-lg h-64 overflow-hidden">
         <a class="text-yellow-500 inline-flex items-center mt-3 h-full w-full" href="<?php echo get_template_directory_uri(); ?>/assets/img/file-download/Attaqi Product Knowledge Brochure.jpg" download="">
            <img alt="content" class="object-cover object-center h-full w-full" src="<?php echo get_template_directory_uri(); ?>/assets/img/thumbnails/Attaqi Product Knowledge Brochure_tn.jpg">
         </a>
        </div>
        <h2 class="text-xl font-medium title-font text-gray-900 mt-5">Product Knowledge Brocure</h2>
        <!-- <p class="text-base leading-relaxed mt-2">Swag shoivdigoitch literally meditation subway tile tumblr cold-pressed. Gastropub street art beard dreamcatcher neutra, ethical XOXO lumbersexual.</p> -->
        <a class="text-yellow-500 inline-flex items-center mt-3" href="<?php echo get_template_directory_uri(); ?>/assets/img/file-download/Attaqi Product Knowledge Brochure.jpg" download="">Download
          <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" class="w-4 h-4 ml-2" viewBox="0 0 24 24">
            <path d="M5 12h14M12 5l7 7-7 7"></path>
          </svg>
        </a>
      </div>
      <div class="p-4 md:w-1/3 sm:mb-0 mb-6">
        <div class="rounded-lg h-64 overflow-hidden">
         <a class="text-yellow-500 inline-flex items-center mt-3 h-full w-full" href="<?php echo get_template_directory_uri(); ?>/assets/img/file-download/Pricelist_595_fixed.pdf" download="">
            <img alt="content" class="object-cover object-center h-full w-full" src="<?php echo get_template_directory_uri(); ?>/assets/img/thumbnails/Pricelist_595_fixed_1.jpg">
         </a>
        </div>
        <h2 class="text-xl font-medium title-font text-gray-900 mt-5">Pricelist</h2>
        <!-- <p class="text-base leading-relaxed mt-2">Swag shoivdigoitch literally meditation subway tile tumblr cold-pressed. Gastropub street art beard dreamcatcher neutra, ethical XOXO lumbersexual.</p> -->
        <a class="text-yellow-500 inline-flex items-center mt-3" href="<?php echo get_template_directory_uri(); ?>/assets/img/file-download/Pricelist_595_fixed.pdf" download="">Download
          <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" class="w-4 h-4 ml-2" viewBox="0 0 24 24">
            <path d="M5 12h14M12 5l7 7-7 7"></path>
          </svg>
        </a>
      </div>
    </div>
  </div>
</section>
<?php get_footer();?>