<?php
/**
 *
 * Template Name: List Building Template
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package attaqitheme
 */

// get_header();
?>

	<!-- <main id="primary" class="site-main"> -->

		<?php
// while (have_posts()):
//  the_post();

//  get_template_part('template-parts/content', 'page');

//  If comments are open or we have at least one comment, load up the comment template.
//  if (comments_open() || get_comments_number()):
//   comments_template();
//  endif;

// endwhile; // End of the loop.

if (empty($_COOKIE['wp_list_building_email'])) {

    ?>

	<?php get_header();?>

<body>
   <section class="text-gray-400 bg-white body-font">
      <form action="/get_posts.php" method="post">
      <div class="container px-5 py-24 mx-auto flex flex-wrap items-center">
         <!-- <div class="bg-yellow-500 p-6 lg:w-3/5 md:w-1/2 md:pr-16 lg:pr-0 pr-0">
            <h1 class="title-font text-3xl text-gray-800 font-bold">Slow-carb next level shoindxgoitch ethical authentic,
               poko scenester</h1>
            <p class="leading-relaxed mt-4 text-white">Poke slow-carb mixtape knausgaard, typewriter street art gentrify hammock
               starladder roathse. Craies vegan tousled etsy austin.</p>
         </div> -->
         <div class="lg:w-4/6 md:w-full bg-gray-100 rounded-lg p-8 flex md:ml-auto w-full mt-10 md:mt-0 mx-auto flex flex-wrap">
            <h2 class="text-gray-900 text-4xl font-bold title-font mb-5 w-full text-center">Booklet & Pricelist Request</h2>
            <div class="relative mb-4 lg:w-1/2 md:w-1/2 w-full px-2">
               <label for="full-name" class="leading-7 text-sm text-gray-600">Nama</label>
               <input required type="text" id="full-name" name="full-name"
                  class="w-full bg-white rounded border border-gray-300 focus:border-yellow-500 focus:ring-2 focus:ring-yellow-200 text-base outline-none text-gray-700 py-1 px-3 leading-8 transition-colors duration-200 ease-in-out">
            </div>
            <div class="relative mb-4 md:w-1/2 md:px-2 px-2 w-full ">
               <label for="email" class="leading-7 text-sm text-gray-600">Email</label>
               <input required type="email" id="email" name="email"
                  class="w-full bg-white rounded border border-gray-300 focus:border-yellow-500 focus:ring-2 focus:ring-yellow-200 text-base outline-none text-gray-700 py-1 px-3 leading-8 transition-colors duration-200 ease-in-out">
            </div>
            <div class="relative mb-4 md:w-1/2 md:px-2 px-2 w-full">
               <label for="address" class="leading-7 text-sm text-gray-600">Alamat</label>
               <textarea required name="address" id="address" cols="30" rows="2" class="w-full bg-white rounded border border-gray-300 focus:border-yellow-500 focus:ring-2 focus:ring-yellow-200 text-base outline-none text-gray-700 py-1 px-3 leading-8 transition-colors duration-200 ease-in-out"></textarea>
            </div>
            <div class="relative mb-4 md:w-1/2 md:px-2 px-2 w-full">
               <label for="phone" class="leading-7 text-sm text-gray-600">Phone</label>
               <input required class="w-full bg-white rounded border border-gray-300 focus:border-yellow-500 focus:ring-2 focus:ring-yellow-200 text-base outline-none text-gray-700 py-1 px-3 leading-8 transition-colors duration-200 ease-in-out"
                     type="tel" id="phone" name="phone" placeholder="62 8123 1234 123" required>
               <!-- <input type="email" id="email" name="email"
                              class="w-full bg-white rounded border border-gray-300 focus:border-yellow-500 focus:ring-2 focus:ring-yellow-200 text-base outline-none text-gray-700 py-1 px-3 leading-8 transition-colors duration-200 ease-in-out"> -->
            </div>
            <div class="relative mb-4 md:w-1/2 md:px-2 px-2 w-full">
               <label for="work" class="leading-7 text-sm text-gray-600">Pekerjaan</label>
               <input required type="text" id="work" name="work"
                  class="w-full bg-white rounded border border-gray-300 focus:border-yellow-500 focus:ring-2 focus:ring-yellow-200 text-base outline-none text-gray-700 py-1 px-3 leading-8 transition-colors duration-200 ease-in-out">
            </div>
            <div class="relative mb-4 md:w-1/2 md:px-2 px-2 w-full">
               <label for="institution" class="leading-7 text-sm text-gray-600">Institusi</label>
               <input required type="text" id="institution" name="institution"
                  class="w-full bg-white rounded border border-gray-300 focus:border-yellow-500 focus:ring-2 focus:ring-yellow-200 text-base outline-none text-gray-700 py-1 px-3 leading-8 transition-colors duration-200 ease-in-out">
            </div>
            <div class="relative mb-4 md:w-1/2 md:px-2 px-2 w-full">

            </div>
            <div class="relative mb-4 md:w-1/2 md:px-2 px-2 w-full">
               <label for="price_range" class="leading-7 text-sm text-gray-600">Mencari Range Harga Rumah : </label>
               <select name="price_range" id="price_range" class="w-full bg-white rounded border border-gray-300 focus:border-yellow-500 focus:ring-2 focus:ring-yellow-200 text-base outline-none text-gray-700 py-1 px-3 leading-8 transition-colors duration-200 ease-in-out">
                  <option value="300-400jt" checked>300 - 400 juta</option>
                  <option value="400-500jt">400 - 500 juta</option>
                  <option value="500-700jt">500 - 700 juta</option>
                  <option value="700-1m">700 - 1 Miliar</option>
                  <option value="1m">> 1 Miliar</option>
               </select>
            </div>
            <div class="relative mb-4 md:w-1/2 md:px-2 px-2 w-full">
               <label for="looking_for_attaqi" class="leading-7 text-sm text-gray-600">Anda mencari produk ATTAQI untuk ?</label>
               <div class="flex flex-col">
                  <div>
                     <input type="checkbox" id="diri_sendiri" name="looking_for_attaqi[]" value="diri_sendiri" checked='true'>
                     <label for="diri_sendiri"> Diri sendiri</label><br>
                  </div>
                  <div>
                     <input type="checkbox" id="keluarga" name="looking_for_attaqi[]" value="keluarga">
                     <label for="keluarga"> Keluarga</label><br>
                  </div>
                  <div>
                     <input type="checkbox" id="investasi" name="looking_for_attaqi[]" value="investasi">
                     <label for="investasi"> Investasi</label>
                  </div>
                  <div>
                     <input type="checkbox" id="teman" name="looking_for_attaqi[]" value="teman">
                     <label for="teman"> Kolega / Teman</label>
                  </div>
                  <div>
                     <input type="checkbox" id="lainnya" name="looking_for_attaqi[]" value="lainnya">
                     <label for="lainnya"> Lainnya</label>
                  </div>
               </div>
            </div>
            <div class="relative mb-4 md:w-1/2 md:px-2 px-2 w-full">
               <label for="know" class="leading-7 text-sm text-gray-600">Saya Mengetahui Attaqi dari : </label>
               <br>
               <div class="flex flex-col">
                  <div>
                     <input type="checkbox" id="instagram" name="reference_from[]" value="instagram" checked>
                     <label for="instagram"> Instagram</label><br>
                  </div>
                  <div>
                     <input type="checkbox" id="Whatsapp" name="reference_from[]" value="whatsapp">
                     <label for="Whatsapp"> Whatsapp</label><br>
                  </div>
                  <div>
                     <input type="checkbox" id="koran" name="reference_from[]" value="koran">
                     <label for="koran"> Koran/Surat Kabar</label>
                  </div>
                  <div>
                     <input type="checkbox" id="iklan_online" name="reference_from[]" value="iklan_online">
                     <label for="iklan_online"> Iklan Online</label>
                  </div>
                  <div>
                     <input type="checkbox" id="agen_marketing" name="reference_from[]" value="agen_marketing">
                     <label for="agen_marketing"> Agen Marketing</label>
                  </div>
               </div>
            </div>
            <button
               type="submit" class="text-white bg-yellow-500 border-0 py-2 px-8 focus:outline-none hover:bg-yellow-600 rounded text-md mx-auto">Kirim</button>
         </div>
      </div>
      </form>
   </section>
   <?php get_footer();?>

<?php
}
// get_sidebar();
// get_footer();
if (!empty($_COOKIE['wp_list_building_email'])) {
    get_template_part('template-parts/download-leaflet');
}
