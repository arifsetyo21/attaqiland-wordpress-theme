module.exports = {
  purge: [
    './*.html',
    './*.php',
    './js/*.js',
    './css/*.css'
  ],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      fill: theme => ({
       'yellow': theme('colors.yellow.500'),
      }),
      spacing: {
        '42': '10.4rem',
        '68': '16.7rem',
        '128': '32rem',
        '144': '36rem',
        '35vh': '35vh',
        '50vh': '50vh',
        '85vh': '85vh',
        '100vh': '100vh'
      }
    },
  },
  variants: {
    extend: {
       fill: ['hover'],
      }
  },
  plugins: [],
}
