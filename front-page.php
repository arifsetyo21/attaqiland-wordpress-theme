   <?php get_header();?>
      <div class="swiper-container gallery-top1 z-0 relative lg:h-85vh h-50vh">
         <div class="swiper-wrapper">
            <div class="swiper-slide">
               <div class="swiper-slide-container">
                  <img class="object-cover object-center rounded" alt="Fasilitas Attaqi Sharia Townhouse" src="<?php echo do_shortcode("[template_dir image='Gallery/fasilitas-1-min.jpg']"); ?>">
               </div>
            </div>
            <div class="swiper-slide">
               <div class="swiper-slide-container">
                  <img class="object-cover object-center rounded" alt="Fasilitas Attaqi Sharia Townhouse" src="<?php echo do_shortcode("[template_dir image='Gallery/fasilitas-2-min.jpg']"); ?>">
               </div>
            </div>
            <div class="swiper-slide">
               <div class="swiper-slide-container">
                  <img class="object-cover object-center rounded" alt="Fasilitas Attaqi Sharia Townhouse" src="<?php echo do_shortcode("[template_dir image='Gallery/fasilitas-3-min.jpg']"); ?>">
               </div>
            </div>
            <div class="swiper-slide">
               <div class="swiper-slide-container">
                  <img class="object-cover object-center rounded" alt="Fasilitas Attaqi Sharia Townhouse" src="<?php echo do_shortcode("[template_dir image='Gallery/R.KELUARGA.jpg']"); ?>">
               </div>
            </div>
            <div class="swiper-slide">
               <div class="swiper-slide-container">
                  <img class="object-cover object-center rounded" alt="Fasilitas Attaqi Sharia Townhouse" src="<?php echo do_shortcode("[template_dir image='Gallery/balai-01-min.jpg']"); ?>">
               </div>
            </div>
         </div>
         <!-- Add Arrows -->
         <div class="swiper-button-next"></div>
         <div class="swiper-button-prev"></div>
      </div>
      <div class="swiper-container gallery-thumbs1 z-10 absolute bottom-35vh lg:bottom-0">
         <div class="swiper-wrapper">
            <div class="swiper-slide">
               <div class="swiper-slide-container">
                  <img class="object-cover object-center rounded-lg" alt="Fasilitas Attaqi Sharia Townhouse" src="<?php echo do_shortcode("[template_dir image='Gallery/fasilitas-1-min.jpg']"); ?>">
               </div>
            </div>
            <div class="swiper-slide">
               <div class="swiper-slide-container">
                  <img class="object-cover object-center rounded" alt="Fasilitas Attaqi Sharia Townhouse" src="<?php echo do_shortcode("[template_dir image='Gallery/fasilitas-2-min.jpg']"); ?>">
               </div>
            </div>
            <div class="swiper-slide">
               <div class="swiper-slide-container">
                  <img class="object-cover object-center rounded" alt="Fasilitas Attaqi Sharia Townhouse" src="<?php echo do_shortcode("[template_dir image='Gallery/fasilitas-3-min.jpg']"); ?>">
               </div>
            </div>
            <div class="swiper-slide">
               <div class="swiper-slide-container">
                  <img class="object-cover object-center rounded" alt="Fasilitas Attaqi Sharia Townhouse" src="<?php echo do_shortcode("[template_dir image='Gallery/R.KELUARGA.jpg']"); ?>">
               </div>
            </div>
            <div class="swiper-slide">
               <div class="swiper-slide-container">
                  <img class="object-cover object-center rounded" alt="Fasilitas Attaqi Sharia Townhouse" src="<?php echo do_shortcode("[template_dir image='Gallery/balai-01-min.jpg']"); ?>">
               </div>
            </div>
         </div>
      </div>
   </div>
   <section class="text-gray-600 body-font mt-10">
      <div class="container px-10 md:pt-4 mx-auto">
         <div class="flex flex-col text-center w-full sm:mb-20 mb-0 mt-20">
            <!-- <h1 class="sm:text-3xl text-2xl font-medium title-font mb-4 text-gray-900">Master Cleanse Reliac Heirloom</h1> -->
            <p class="lg:w-2/3 mx-auto leading-relaxed text-base font-semibold text-md sm:text-2xl mb-5">Attaqi Land berkomitmen untuk
               menciptakan hunian berupa perumahan dan cluster Muslim, dengan pembayaran skema 100%
               Syariah tanpa riba dan menciptakan lingkungan yang berorientasi Dunia dan Akhirat.</p>
            <div class="md:flex md:flex-row flex-col justify-center hidden ">
               <a href="/#video-preview"
                  class="text-white border-0 py-2 px-6 focus:outline-none bg-yellow-500 hover:text-yellow-500 hover:border-2 hover:border-yellow-500 hover:bg-white hover:text-bold hover:border-4 border rounded-full text-lg md:mr-2 md:mb-0 mb-2">Lihat
                  Video Preview</a>
               <a href="/#faq"
                  class="text-white border-0 py-2 px-6 focus:outline-none bg-yellow-500 hover:text-yellow-500 hover:border-2 hover:border-yellow-500 hover:bg-white hover:text-bold hover:border-4 border rounded-full text-lg md:ml-2">
                  Frequently Asked Question</a>
            </div>
         </div>
      </div>
   </section>
   <section class="text-gray-600 body-font sm:mt-10 mt-0" id="feature">
      <div class="container md:px-16 px-12 py-0 mx-auto">
         <div class="flex flex-col text-center w-full mb-20 sm:mt-20">
            <div class="text-white bg-yellow-500 mb-5 md:rounded-full rounded-3xl sm:p-0 p-5">
               <h1 class="title-font md:text-3xl font-semibold p-2 pt-4 inline-block text-2xl">
                  Keunggulan ATTAQI LUXURY SHARIA TOWNHOUSE dari berbagai perspektif</h1>
               <h3 class="p-2 pb-3">*klik yang ingin anda lihat</h3>
            </div>
            <div class="flex flex-wrap -m-4">
               <div class="lg:w-1/3 sm:w-1/2 p-4 rounded order-1 lg:order-1" data-aos="fade-up">
                  <a href="#designandtype" class="flex relative rounded">
                     <!-- <div class="relative pt-4 pb-4 pl-4 pr-11 z-10 w-1/2 bg-white rounded-l">
                        <div class="h-1 bg-gray-200 rounded overflow-hidden">
                           <div class="w-24 h-full bg-yellow-500"></div>
                        </div>
                        <h2 class="tracking-widest text-left text-lg title-font font-medium text-yellow-500 mb-16 mt-4">#1</h2>
                        <h1 class="title-font text-lg font-semibold text-gray-900 text-left">DESAIN & TIPE RUMAH</h1>
                        <p class="leading-relaxed"></p>
                     </div> -->
                     <img alt="Keunggulan 1 Desain dan Type Rumah" class="relative inset-0 w-full object-scale-down object-center rounded"
                        src="<?php echo do_shortcode("[template_dir image='keunggulan/keunggulan-1-min.jpg']"); ?>">
                  </a>
               </div>
               <div class="lg:w-1/3 sm:w-1/2 p-4 rounded order-2 lg:order-4" data-aos="fade-up">
                  <a href="#lokasiandlingkungan" class="flex relative rounded">
                     <img alt="Keunggulan 2 Lokasi dan Lingkungan" class="relative inset-0 w-full object-scale-down object-center rounded" src="<?php echo do_shortcode("[template_dir image='keunggulan/keunggulan-2-min.jpg']"); ?>">
                  </a>
               </div>
               <div class="lg:w-1/3 sm:w-1/2 p-4 rounded order-3 lg:order-2" data-aos="fade-up">
                  <a href="#materialgradeterbaik" class="flex relative rounded">
                     <img alt="Keunggulan 3 Material Grade Terbaik" class="relative inset-0 w-full object-scale-down object-center rounded" src="<?php echo do_shortcode("[template_dir image='keunggulan/keunggulan-3-min.jpg']"); ?>">
                  </a>
               </div>
               <div class="lg:w-1/3 sm:w-1/2 p-4 rounded order-4 lg:order-5" data-aos="fade-up">
                  <a href="#fasilitas" class="flex relative rounded">
                     <img alt="Keunggulan 4 Fasilitas Khusus yang Lengkap" class="relative inset-0 w-full object-scale-down object-center rounded" src="<?php echo do_shortcode("[template_dir image='keunggulan/keunggulan-4-min.jpg']"); ?>">
                  </a>
               </div>
               <div class="lg:w-1/3 sm:w-1/2 p-4 rounded order-5 lg:order-3" data-aos="fade-up">
                  <a href="#fullyfurnishedservice" class="flex relative rounded">
                     <img alt="Keunggulan 5 ATTAQI FULLY FURNISHED SERVICE Adalah layanan untuk para pembeli rumah dan penghuni, yang mau mengisi rumahnya dengan perabotan, mulai dari per ruangan atau secara keseluruhan (fully furnished)" class="relative inset-0 w-full object-scale-down object-center rounded" src="<?php echo do_shortcode("[template_dir image='keunggulan/keunggulan-5-min.jpg']"); ?>">
                  </a>
               </div>
               <div class="lg:w-1/3 sm:w-1/2 p-4 rounded order-6 lg:order-6" data-aos="fade-up">
                  <a href="#pelayanan" class="flex relative rounded">
                     <img alt="Keunggulan 6 AFTER SALES, DESIGN REQUEST, DESIGN & BUILD, PENGADAAN PERABOTAN RUMAH TANGGA" class="relative inset-0 w-full object-scale-down object-center rounded" src="<?php echo do_shortcode("[template_dir image='keunggulan/keunggulan-6-min.jpg']"); ?>">
                  </a>
               </div>
            </div>
         </div>
      </div>
   </section>
   <section class="text-gray-600 bg-blue-550 body-font bg-white">
      <div class="container md:px-16 px-10 py-24 mx-auto">
         <div class="flex flex-col">
            <div class="h-1 bg-gray-500 rounded overflow-hidden">
               <div class="w-24 h-full bg-yellow-500"></div>
            </div>
            <div class="flex flex-wrap sm:flex-row flex-col py-6 md:mb-12">
               <h1
                  class="sm:w-2/5 text-white font-bold title-font text-4xl mb-2 sm:mb-0 md:text-left text-center md:mb-0 md:mb-10">
                  Progres Pembangunan Terbaru</h1>
               <!-- <p class="sm:w-3/5 leading-relaxed text-white sm:pl-10 pl-0 text-lg md:text-left text-center">Awal keluarga
                     harmonis, desain yang elegan dan
                     compact, serta fitur modern memberikan
                     kenyamanan optimal.</p> -->
            </div>
         </div>
         <div class="flex md:flex-row flex-col flex-wrap -m-4">
            <div class="lg:w-full sm:w-full p-4" data-aos="fade-up">
               <div class="flex relative">
                  <img alt="Attaqi Sharia Townhouse Foto Desain Type Basic"
                     class="absolute inset-0 w-full h-full object-cover object-center rounded"
                     src="<?php echo do_shortcode("[template_dir image='progres/5_22-mei-2021_paving-jalan-lin.jpg']"); ?>">
                  <a href="<?php echo do_shortcode("[template_dir image='progres/5_22-mei-2021_paving-jalan-lin.jpg']"); ?>" data-lightbox="basicDesign" data-title=""
                     class="px-0 lg:py-64 md:py-32 py-16 relative z-10 w-full bg-white opacity-0 hover:opacity-100 text-center rounded">
                     <h1 class="title-font text-lg font-medium text-gray-900 mb-3">22 Mei 2021 - Paving Jalan Lingkungan</h1>
                  </a>
               </div>
            </div>
         </div>
         <div class="flex justify-center py-10">
            <a href="/progress"
               class="inline-flex text-white border-opacity-0 py-2 px-6 bg-yellow-500 hover:text-yellow-500 hover:border-2 hover:border-opacity-100 hover:border-yellow-500 hover:bg-white hover:text-bold rounded-full text-lg">LIHAT PROGRES LENGKAP</a>
         </div>
      </div>
   </section>
   <section class="text-gray-600 body-font" id="designandtype">
      <div class="container md:px-16 px-10 py-24 mx-auto">
         <div class="flex flex-col">
            <div class="h-1 bg-gray-200 rounded overflow-hidden">
               <div class="w-24 h-full bg-yellow-500"></div>
            </div>
            <div class="flex flex-wrap sm:flex-row flex-col py-6 mb-12 justify-between items-center">
               <h1 class="sm:w-2/5 text-gray-900 font-extrabold title-font text-5xl mb-10 sm:mb-0">DESAIN & <br>TIPE
                  RUMAH
               </h1>
               <a href="https://www.instagram.com/attaqi_land/" target="_blank" rel="nofollow"
                  class="call-attaqi-btn inline-flex text-white border-0 py-2 px-6 align-middle focus:outline-none bg-yellow-500 hover:text-yellow-500 hover:border-2 hover:border-yellow-500 hover:bg-white hover:text-bold hover:border-4 border rounded-full text-lg ml-2">
                  Hubungi @attaqi_land&nbsp<svg class="ig-icon-svg fill-current text-white mt-1" xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24">
                     <path
                        d="M12 2.163c3.204 0 3.584.012 4.85.07 3.252.148 4.771 1.691 4.919 4.919.058 1.265.069 1.645.069 4.849 0 3.205-.012 3.584-.069 4.849-.149 3.225-1.664 4.771-4.919 4.919-1.266.058-1.644.07-4.85.07-3.204 0-3.584-.012-4.849-.07-3.26-.149-4.771-1.699-4.919-4.92-.058-1.265-.07-1.644-.07-4.849 0-3.204.013-3.583.07-4.849.149-3.227 1.664-4.771 4.919-4.919 1.266-.057 1.645-.069 4.849-.069zm0-2.163c-3.259 0-3.667.014-4.947.072-4.358.2-6.78 2.618-6.98 6.98-.059 1.281-.073 1.689-.073 4.948 0 3.259.014 3.668.072 4.948.2 4.358 2.618 6.78 6.98 6.98 1.281.058 1.689.072 4.948.072 3.259 0 3.668-.014 4.948-.072 4.354-.2 6.782-2.618 6.979-6.98.059-1.28.073-1.689.073-4.948 0-3.259-.014-3.667-.072-4.947-.196-4.354-2.617-6.78-6.979-6.98-1.281-.059-1.69-.073-4.949-.073zm0 5.838c-3.403 0-6.162 2.759-6.162 6.162s2.759 6.163 6.162 6.163 6.162-2.759 6.162-6.163c0-3.403-2.759-6.162-6.162-6.162zm0 10.162c-2.209 0-4-1.79-4-4 0-2.209 1.791-4 4-4s4 1.791 4 4c0 2.21-1.791 4-4 4zm6.406-11.845c-.796 0-1.441.645-1.441 1.44s.645 1.44 1.441 1.44c.795 0 1.439-.645 1.439-1.44s-.644-1.44-1.439-1.44z" />
                  </svg></a>
            </div>
         </div>
         <div class="flex flex-wrap sm:-m-4 -mx-4 -mb-10 -mt-4">
            <div class="md:p-10 md:w-1/2 sm:mb-0 mb-6" data-aos="fade-up">
               <div class="rounded overflow-hidden">
                  <img alt="Desain compact, Elegan dan Modern, Efisiensi ruang optimal." class="object-scale-down object-center h-full w-full" src="<?php echo do_shortcode("[template_dir image='desain/desain-1-thumb-min.png']"); ?>">
               </div>
               <p class="text-base leading-relaxed mt-2 text-justify pt-4">Desain compact, Elegan dan Modern,
                  Efisiensi ruang optimal, mengurangi biaya
                  perawatan, dan meningkatkan efisiensi
                  aktivitas dengan aplikasi SMARTHOME.</p>
            </div>
            <div class="md:p-10 md:w-1/2 sm:mb-0 mb-6" data-aos="fade-up">
               <div class="rounded overflow-hidden">
                  <img alt="Sirkulasi udara dan pencahayaan optimal, menjadi kunci hunian sehat." class="object-scale-down object-center h-full w-full" src="<?php echo do_shortcode("[template_dir image='desain/desain-2-thumb-min.png']"); ?>">
               </div>
               <p class="text-base leading-relaxed mt-2 text-justify pt-4">Sirkulasi udara dan pencahayaan optimal,
                  menjadi kunci hunian sehat, ditambah
                  banyaknya bukaan di setiap sisi rumah dan
                  jendela yang lebar.</p>
            </div>
            <div class="md:p-10 md:w-1/2 sm:mb-0 mb-6" data-aos="fade-up">
               <div class="rounded overflow-hidden">
                  <img alt="Mini garden / Inner court, memberikan suasana semi tropis dan jadi tempat refreshing kecil-kecilan." class="object-scale-down object-center h-full w-full" src="<?php echo do_shortcode("[template_dir image='desain/desain-3-thumb-min.png']"); ?>">
               </div>
               <p class="text-base leading-relaxed mt-2 text-justify pt-4">Mini garden / Inner court, memberikan
                  suasana semi tropis dan jadi tempat
                  refreshing kecil-kecilan.</p>
            </div>
            <div class="md:p-10 md:w-1/2 sm:mb-0 mb-6" data-aos="fade-up">
               <div class="rounded overflow-hidden">
                  <img alt="Mini garden / Inner court, memberikan Tata letak dan jangkauan antar ruang yang optimal serta aman." class="object-scale-down object-center h-full w-full" src="<?php echo do_shortcode("[template_dir image='desain/desain-4-thumb-min.png']"); ?>">
               </div>
               <p class="text-base leading-relaxed mt-2 text-justify pt-4">Mini garden / Inner court, memberikan
                  Tata letak dan jangkauan antar ruang yang optimal serta aman,
                  memberikan kemudahan akses dan efisiensi energi bagi
                  penghuninya.</p>
            </div>
            <div class="md:p-10 md:w-1/2 sm:mb-0 mb-6" data-aos="fade-up">
               <div class="rounded overflow-hidden">
                  <img alt="Desain vs Real, setiap realisasi pembangunan diawasi dengan ketat." class="object-scale-down object-center h-full w-full" src="<?php echo do_shortcode("[template_dir image='desain/desain-5-thumb-min.png']"); ?>">
               </div>
               <p class="text-base leading-relaxed mt-2 text-justify pt-4">Desain vs Real, setiap realisasi
                  pembangunan diawasi dengan ketat dan
                  benar-benar hanya menggunakan material
                  terbaik serta supplier dengan mutu yang
                  sudah teruji, sehingga menghasilkan
                  bangunan yang sesuai ekspektasi.</p>
            </div>
            <div class="md:p-10 md:w-1/2 sm:mb-0 mb-6" data-aos="fade-up">
               <div class="rounded overflow-hidden">
                  <img alt="Didesain dan dibangun oleh arsitek serta kontraktor berpengalaman." class="object-scale-down object-center h-full w-full" src="<?php echo do_shortcode("[template_dir image='desain/desain-6-thumb-min.png']"); ?>">
               </div>
               <p class="text-base leading-relaxed mt-2 text-justify pt-4">Didesain dan dibangun oleh arsitek serta
                  kontraktor berpengalaman, setiap inci lahan
                  dan bangunan diperhatikan detail dan
                  eksekusinya.</p>
            </div>
         </div>
      </div>
   </section>
   <section class="text-gray-600 body-font bg-blue-550">
      <div class="container md:px-16 px-10 py-24 mx-auto">
         <div class="flex flex-col">
            <div class="h-1 bg-gray-500 rounded overflow-hidden">
               <div class="w-24 h-full bg-yellow-500"></div>
            </div>
            <div class="flex flex-wrap sm:flex-row flex-col py-6 mb-12">
               <h1 class="sm:w-2/5 text-white font-bold title-font text-4xl mb-2 sm:mb-0 md:text-left text-center md:mb-0 mb-10">Desain TYPE BASIC |<br>LB/LT (64/78)</h1>
               <p class="sm:w-3/5 leading-relaxed text-white sm:pl-10 pl-0 text-lg md:text-left text-center">Awal keluarga harmonis, desain yang elegan dan
               compact, serta fitur modern memberikan
               kenyamanan optimal.</p>
            </div>
         </div>
         <div class="flex md:flex-row flex-col flex-wrap -m-4">
            <div class="lg:w-1/3 sm:w-1/2 p-4" data-aos="fade-up">
               <div class="flex relative">
                  <img alt="Attaqi Sharia Townhouse Foto Desain Type Basic" class="absolute inset-0 w-full h-full object-scale-down object-center rounded"
                     src="<?php echo do_shortcode("[template_dir image='basic/basic-1-thumb-min.jpg']"); ?>">

                  <a href="<?php echo do_shortcode("[template_dir image='basic/basic-1.png']"); ?>" data-lightbox="basicDesign" data-title=""
                     class="py-40 relative z-10 w-full bg-white opacity-0 hover:opacity-100 text-center rounded">
                     <h1 class="title-font text-lg font-medium text-gray-900 mb-3">FOTO BASIC 1</h1>
                  </a>
               </div>
            </div>
            <div class="lg:w-1/3 sm:w-1/2 p-4" data-aos="fade-up">
               <div class="flex relative">
                  <img class="absolute inset-0 w-full h-full object-scale-down object-center rounded"
                     src="<?php echo do_shortcode("[template_dir image='basic/basic-2-thumb-min.jpg']"); ?>" alt="Attaqi Sharia Townhouse Foto Desain Type Basic" />
                  <a href="<?php echo do_shortcode("[template_dir image='basic/basic-2.png']"); ?>" data-lightbox="basicDesign"
                     data-title=""
                     class="py-40 relative z-10 w-full bg-white opacity-0 hover:opacity-100 text-center rounded">
                     <h1 class="title-font text-lg font-medium text-gray-900 mb-3">FOTO BASIC 2</h1>
                  </a>
               </div>
            </div>
            <div class="lg:w-1/3 sm:w-1/2 p-4" data-aos="fade-up">
               <div class="flex relative">
                  <img alt="Attaqi Sharia Townhouse Foto Desain Type Basic" class="absolute inset-0 w-full h-full object-scale-down object-center rounded"
                     src="<?php echo do_shortcode("[template_dir image='basic/basic-3-thumb-min.jpg']"); ?>">
                  <a href="<?php echo do_shortcode("[template_dir image='basic/basic-3.png']"); ?>" data-lightbox="basicDesign"
                     data-title=""
                     class="py-40 relative z-10 w-full bg-white opacity-0 hover:opacity-100 text-center rounded">
                     <h1 class="title-font text-lg font-medium text-gray-900 mb-3">FOTO BASIC 3</h1>
                  </a>
               </div>
            </div>
            <div class="lg:w-1/3 sm:w-1/2 p-4" data-aos="fade-up">
               <div class="flex relative">
                  <img alt="Attaqi Sharia Townhouse Foto Desain Type Basic" class="absolute inset-0 w-full h-full object-scale-down object-center rounded"
                     src="<?php echo do_shortcode("[template_dir image='basic/basic-4-thumb-min.jpg']"); ?>">
                  <a href="<?php echo do_shortcode("[template_dir image='basic/basic-4.png']"); ?>" data-lightbox="basicDesign" data-title=""
                     class="py-40 relative z-10 w-full bg-white opacity-0 hover:opacity-100 text-center rounded">
                     <h1 class="title-font text-lg font-medium text-gray-900 mb-3">FOTO BASIC 4</h1>
                  </a>
               </div>
            </div>
            <div class="lg:w-1/3 sm:w-1/2 p-4" data-aos="fade-up">
               <div class="flex relative">
                  <img alt="Attaqi Sharia Townhouse Foto Desain Type Basic" class="absolute inset-0 w-full h-full object-scale-down object-center rounded"
                     src="<?php echo do_shortcode("[template_dir image='basic/basic-5-thumb-min.jpg']"); ?>">
                  <a href="<?php echo do_shortcode("[template_dir image='basic/basic-5.png']"); ?>" data-lightbox="basicDesign" data-title=""
                     class="py-40 relative z-10 w-full bg-white opacity-0 hover:opacity-100 text-center rounded">
                     <h1 class="title-font text-lg font-medium text-gray-900 mb-3">FOTO BASIC 5</h1>
                  </a>
               </div>
            </div>
            <div class="lg:w-1/3 sm:w-1/2 p-4" data-aos="fade-up">
               <div class="flex relative">
                  <img alt="Attaqi Sharia Townhouse Foto Desain Type Basic" class="absolute inset-0 w-full h-full object-scale-down object-center rounded"
                     src="<?php echo do_shortcode("[template_dir image='basic/basic-6-thumb-min.jpg']"); ?>">
                  <a href="<?php echo do_shortcode("[template_dir image='basic/basic-6.png']"); ?>" data-lightbox="basicDesign" data-title=" "
                     class="py-40 relative z-10 w-full bg-white opacity-0 hover:opacity-100 text-center rounded">
                     <h1 class="title-font text-lg font-medium text-gray-900 mb-3">FOTO BASIC 6</h1>
                  </a>
               </div>
            </div>
            <div class="lg:w-full sm:w-full p-4" data-aos="fade-up">
               <div class="flex relative">
                  <img alt="Attaqi Sharia Townhouse Foto Desain Type Basic" class="absolute inset-0 w-full h-full object-cover object-center rounded"
                     src="<?php echo do_shortcode("[template_dir image='basic/basic-7-thumb-min.jpg']"); ?>">
                  <a href="<?php echo do_shortcode("[template_dir image='basic/basic-7.png']"); ?>" data-lightbox="basicDesign" data-title=""
                     class="px-8 py-32 relative z-10 w-full bg-white opacity-0 hover:opacity-100 text-center rounded">
                     <h1 class="title-font text-lg font-medium text-gray-900 mb-3">FOTO BASIC 7</h1>
                  </a>
               </div>
            </div>
         </div>
         <div class="flex justify-center py-10">
            <a href="<?php echo do_shortcode("[template_dir image='denah/DENAH BASIC.png']"); ?>" data-lightbox="basicDesignSitePlan" data-title="Denah Type Basic | LB/LT (64/78)"
               class="inline-flex text-white border-opacity-0 py-2 px-6 bg-yellow-500 hover:text-yellow-500 hover:border-2 hover:border-opacity-100 hover:border-yellow-500 hover:bg-white hover:text-bold rounded-full text-lg">LIHAT DENAH TYPE BASIC</a>
         </div>
      </div>
   </section>
   <section class="text-gray-600 body-font">
      <div class="container md:px-16 px-10 py-24 mx-auto">
         <div class="flex flex-col">
            <div class="h-1 bg-gray-200 rounded overflow-hidden">
               <div class="w-24 h-full bg-yellow-500"></div>
            </div>
            <div class="flex flex-wrap sm:flex-row flex-col py-6 mb-12">
               <h1 class="sm:w-2/5 text-gray-900 font-bold title-font md:text-3xl text-3xl mb-2 sm:mb-0 md:text-left text-center mb-10 md:mb-0">Desain TYPE DELUXE A| LB/LT (81/78)
                  <br>
               & <br>
               Desain TYPE DELUXE B| LB/LT (92/91)</h1>
               <p class="sm:w-3/5 leading-relaxed text-lg sm:pl-10 pl-0 md:text-left text-center">2 Pilihan ideal untuk keluarga tercinta, akses ruangan
               yang lega dan nyaman.</p>
            </div>
         </div>
         <div class="flex md:flex-row flex-col flex-wrap -m-4">
            <div class="lg:w-1/2 sm:w-1/2 p-4" data-aos="fade-up">
               <div class="flex relative">
                  <img alt="Attaqi Sharia Townhouse Foto Desain Type Deluxe" class="absolute inset-0 w-full h-full object-scale-down object-center rounded"
                     src="<?php echo do_shortcode("[template_dir image='deluxe/deluxe-1-thumb-min.jpg']"); ?>">

                  <a href="<?php echo do_shortcode("[template_dir image='deluxe/deluxe-1.png']"); ?>" data-lightbox="deluxImage"
                     data-title=""
                     class="px-8 md:py-42 py-24 relative z-10 w-full bg-white opacity-0 hover:opacity-100 text-center">
                     <h1 class="title-font text-lg font-medium text-gray-900 mb-3">FOTO DELUXE 1</h1>
                  </a>
               </div>
            </div>
            <div class="lg:w-1/2 sm:w-1/2 p-4" data-aos="fade-up">
               <div class="flex relative">
                  <img class="absolute inset-0 w-full h-full object-scale-down object-center rounded"
                     src="<?php echo do_shortcode("[template_dir image='deluxe/deluxe-2-thumb-min.jpg']"); ?>" alt="Attaqi Sharia Townhouse Foto Desain Type Deluxe" />
                  <a href="<?php echo do_shortcode("[template_dir image='deluxe/deluxe-2.png']"); ?>" data-lightbox="deluxImage"
                     data-title=""
                     class="px-8 md:py-42 py-24 relative z-10 w-full bg-white opacity-0 hover:opacity-100 text-center">
                     <h1 class="title-font text-lg font-medium text-gray-900 mb-3">FOTO DELUXE 2</h1>
                  </a>
               </div>
            </div>
            <div class="lg:w-1/2 sm:w-1/2 p-4" data-aos="fade-up">
               <div class="flex relative">
                  <img alt="Attaqi Sharia Townhouse Foto Desain Type Deluxe" class="absolute inset-0 w-full h-full object-scale-down object-center rounded"
                     src="<?php echo do_shortcode("[template_dir image='deluxe/deluxe-3-thumb-min.jpg']"); ?>">
                  <a href="<?php echo do_shortcode("[template_dir image='deluxe/deluxe-3.png']"); ?>" data-lightbox="deluxImage"
                     data-title=""
                     class="px-8 md:py-42 py-24 relative z-10 w-full bg-white opacity-0 hover:opacity-100 text-center">
                     <h1 class="title-font text-lg font-medium text-gray-900 mb-3">FOTO DELUXE 3</h1>
                  </a>
               </div>
            </div>
            <div class="lg:w-1/2 sm:w-1/2 p-4" data-aos="fade-up">
               <div class="flex relative">
                  <img alt="Attaqi Sharia Townhouse Foto Desain Type Deluxe" class="absolute inset-0 w-full h-full object-scale-down object-center rounded"
                     src="<?php echo do_shortcode("[template_dir image='deluxe/deluxe-4-thumb-min.jpg']"); ?>">
                  <a href="<?php echo do_shortcode("[template_dir image='deluxe/deluxe-4.png']"); ?>" data-lightbox="deluxImage" data-title=""
                     class="px-8 md:py-42 py-24 relative z-10 w-full bg-white opacity-0 hover:opacity-100 text-center">
                     <h1 class="title-font text-lg font-medium text-gray-900 mb-3">FOTO DELUXE 4</h1>
                  </a>
               </div>
            </div>
         </div>
         <div class="flex justify-center py-10 text-center">
            <a href="<?php echo do_shortcode("[template_dir image='denah/DENAH DELUXE A.png']"); ?>" data-lightbox="deluxeMapsImage" data-title="DENAH TYPE DELUXE A| LB/LT (81/78)"
               class="inline-flex text-white border-opacity-0 py-2 px-6 bg-yellow-500 hover:text-yellow-500 hover:border-2 hover:border-opacity-100 hover:border-yellow-500 hover:bg-white hover:text-bold rounded-full text-lg">LIHAT
               GALERI TYPE DELUXE A/B</a>
               <a href="<?php echo do_shortcode("[template_dir image='denah/DENAH DELUXE B.png']"); ?>" data-lightbox="deluxeMapsImage" data-title="DENAH TYPE DELUXE B| LB/LT (92/91)" hidden></a>
         </div>
      </div>
   </section>
   <section class="text-gray-600 body-font bg-blue-550">
      <div class="container md:px-16 px-10 py-24 mx-auto">
         <div class="flex flex-col">
            <div class="h-1 bg-gray-500 rounded overflow-hidden">
               <div class="w-24 h-full bg-yellow-500"></div>
            </div>
            <div class="flex flex-wrap sm:flex-row flex-col py-6 mb-12">
               <h1 class="sm:w-2/5 text-white font-bold title-font text-4xl mb-2 sm:mb-0 md:text-left text-center md:mb-0 mb-10">TYPE LUXURY (SOHO) |<br>LB/LT (92/91)</h1>
               <p class="sm:w-3/5 leading-relaxed text-white sm:pl-10 pl-0 text-lg md:text-left text-center">Small Office Home Office, Rumah penuh produktifitas, eksklusifitas dan Kenyamanan.</p>
            </div>
         </div>
         <div class="flex md:flex-row flex-col flex-wrap -m-4">
            <div class="lg:w-1/2 sm:w-1/2 p-4" data-aos="fade-up">
               <div class="flex relative">
                  <img alt="Attaqi Sharia Townhouse Foto Desain Type Luxury" class="absolute inset-0 w-full h-full object-scale-down object-center rounded"
                     src="<?php echo do_shortcode("[template_dir image='luxury/luxury-1-thumb-min.jpg']"); ?>">

                  <a href="<?php echo do_shortcode("[template_dir image='luxury/LUXURY 1.png']"); ?>" data-lightbox="luxuryDesign" data-title=""
                     class="px-8 md:py-42 py-24 relative z-10 w-full bg-white opacity-0 hover:opacity-100 text-center">
                     <h1 class="title-font text-lg font-medium text-gray-900 mb-3">FOTO LUXURY 1</h1>
                  </a>
               </div>
            </div>
            <div class="lg:w-1/2 sm:w-1/2 p-4" data-aos="fade-up">
               <div class="flex relative">
                  <img class="absolute inset-0 w-full h-full object-scale-down object-center rounded"
                        src="<?php echo do_shortcode("[template_dir image='luxury/luxury-2-thumb-min.jpg']"); ?>" alt="Attaqi Sharia Townhouse Foto Desain Type Luxury" />
                  <a href="<?php echo do_shortcode("[template_dir image='luxury/LUXURY 2.png']"); ?>" data-lightbox="luxuryDesign"
                     data-title=""
                     class="px-8 md:py-42 py-24 relative z-10 w-full bg-white opacity-0 hover:opacity-100 text-center">
                     <h1 class="title-font text-lg font-medium text-gray-900 mb-3">FOTO LUXURY 2</h1>
                  </a>
               </div>
            </div>
            <div class="lg:w-1/2 sm:w-1/2 p-4" data-aos="fade-up">
               <div class="flex relative">
                  <img alt="Attaqi Sharia Townhouse Foto Desain Type Luxury" class="absolute inset-0 w-full h-full object-scale-down object-center rounded"
                     src="<?php echo do_shortcode("[template_dir image='luxury/luxury-3-thumb-min.jpg']"); ?>">
                  <a href="<?php echo do_shortcode("[template_dir image='luxury/LUXURY 3.png']"); ?>" data-lightbox="luxuryDesign"
                     data-title=""
                     class="px-8 md:py-42 py-24 relative z-10 w-full bg-white opacity-0 hover:opacity-100 text-center">
                     <h1 class="title-font text-lg font-medium text-gray-900 mb-3">FOTO LUXURY 3</h1>
                  </a>
               </div>
            </div>
            <div class="lg:w-1/2 sm:w-1/2 p-4" data-aos="fade-up">
               <div class="flex relative">
                  <img alt="Attaqi Sharia Townhouse Foto Desain Type Luxury" class="absolute inset-0 w-full h-full object-scale-down object-center rounded"
                     src="<?php echo do_shortcode("[template_dir image='luxury/luxury-4-thumb-min.jpg']"); ?>">
                  <a href="<?php echo do_shortcode("[template_dir image='luxury/LUXURY 4.png']"); ?>" data-lightbox="luxuryDesign" data-title=""
                     class="px-8 md:py-42 py-24 relative z-10 w-full bg-white opacity-0 hover:opacity-100 text-center">
                     <h1 class="title-font text-lg font-medium text-gray-900 mb-3">FOTO LUXURY 4</h1>
                  </a>
               </div>
            </div>
         </div>
         <div class="flex justify-center py-10">
            <a href="<?php echo do_shortcode("[template_dir image='denah/DENAH LUXURY.png']"); ?>" data-lightbox="luxuryMapsImage" data-title="Denah TYPE LUXURY (SOHO) | LB/LT (92/91)"
               class="inline-flex text-white border-opacity-0 py-2 px-6 bg-yellow-500 hover:text-yellow-500 hover:border-2 hover:border-opacity-100 hover:border-yellow-500 hover:bg-white hover:text-bold rounded-full text-lg">LIHAT DENAH TYPE LUXURY</a>
         </div>
      </div>
   </section>
   <section class="text-gray-600 body-font" id="lokasiandlingkungan">
      <div class="container md:px-16 px-10 py-24 mx-auto">
         <div class="flex flex-col">
            <div class="h-1 bg-gray-200 rounded overflow-hidden">
               <div class="w-24 h-full bg-yellow-500"></div>
            </div>
            <div class="flex flex-wrap sm:flex-row flex-col py-6 mb-12 justify-between items-center">
               <h1 class="sm:w-2/5 text-gray-900 font-extrabold title-font text-5xl mb-2 sm:mb-0 mb-10 text-center sm:text-left">LOKASI & <br> LINGKUNGAN
               </h1>
               <a href="https://www.instagram.com/attaqi_land/" target="_blank" rel="nofollow"
                  class="call-attaqi-btn inline-flex text-white border-0 py-2 px-6 align-middle focus:outline-none bg-yellow-500 hover:text-yellow-500 hover:border-2 hover:border-yellow-500 hover:bg-white hover:text-bold hover:border-4 border rounded-full text-lg ml-2">
                  Hubungi @attaqi_land&nbsp<svg class="ig-icon-svg fill-current text-white mt-1"
                     xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24">
                     <path
                        d="M12 2.163c3.204 0 3.584.012 4.85.07 3.252.148 4.771 1.691 4.919 4.919.058 1.265.069 1.645.069 4.849 0 3.205-.012 3.584-.069 4.849-.149 3.225-1.664 4.771-4.919 4.919-1.266.058-1.644.07-4.85.07-3.204 0-3.584-.012-4.849-.07-3.26-.149-4.771-1.699-4.919-4.92-.058-1.265-.07-1.644-.07-4.849 0-3.204.013-3.583.07-4.849.149-3.227 1.664-4.771 4.919-4.919 1.266-.057 1.645-.069 4.849-.069zm0-2.163c-3.259 0-3.667.014-4.947.072-4.358.2-6.78 2.618-6.98 6.98-.059 1.281-.073 1.689-.073 4.948 0 3.259.014 3.668.072 4.948.2 4.358 2.618 6.78 6.98 6.98 1.281.058 1.689.072 4.948.072 3.259 0 3.668-.014 4.948-.072 4.354-.2 6.782-2.618 6.979-6.98.059-1.28.073-1.689.073-4.948 0-3.259-.014-3.667-.072-4.947-.196-4.354-2.617-6.78-6.979-6.98-1.281-.059-1.69-.073-4.949-.073zm0 5.838c-3.403 0-6.162 2.759-6.162 6.162s2.759 6.163 6.162 6.163 6.162-2.759 6.162-6.163c0-3.403-2.759-6.162-6.162-6.162zm0 10.162c-2.209 0-4-1.79-4-4 0-2.209 1.791-4 4-4s4 1.791 4 4c0 2.21-1.791 4-4 4zm6.406-11.845c-.796 0-1.441.645-1.441 1.44s.645 1.44 1.441 1.44c.795 0 1.439-.645 1.439-1.44s-.644-1.44-1.439-1.44z" />
                  </svg></a>
            </div>
         </div>
         <div class="flex flex-wrap sm:-m-4 -mx-4 -mb-10 -mt-4">
            <div class="md:p-6 p-4 md:w-1/2  mb-2" data-aos="fade-up">
               <a  href="<?php echo do_shortcode("[template_dir image='lokasi/LOKASI 1.png']"); ?>" data-lightbox="lokasiandlingkungan" data-title="">
                  <div class="rounded overflow-hidden">
                     <img alt="Foto Attaqi Sharia Townhouse adalah salah satu kelurahan paling maju di jawa tengah dan Solo Raya" class="object-cover object-center h-full w-full" src="<?php echo do_shortcode("[template_dir image='lokasi/lokasi-1-thumb-min.jpg']"); ?>">
                  </div>
               </a>
               <p class="text-base leading-relaxed mt-2 text-justify">Gentan adalah salah satu kelurahan paling
               maju di jawa tengah dan Solo Raya, berada
               di perbatasan kota Solo dan Kabupaten
               Sukoharjo, memberi akses mudah ke
               berbagai fasilitas yang serba ada.</p>
            </div>
            <div class="md:p-6 p-4 md:w-1/2 mb-2" data-aos="fade-up">
               <a href="<?php echo do_shortcode("[template_dir image='lokasi/lokasi-2.png']"); ?>" data-lightbox="lokasiandlingkungan" data-title="">
                  <div class="rounded overflow-hidden">
                     <img alt="Foto Attaqi Sharia Townhouse Kawasan Residensial dengan penduduk yang ramah, serta UMKM yang melimpah" class="object-cover object-center h-full w-full"
                        src="<?php echo do_shortcode("[template_dir image='lokasi/lokasi-2-thumb-min.jpg']"); ?>">
                  </div>
               </a>
               <p class="text-base leading-relaxed mt-2 text-justify">Kawasan Residensial dengan penduduk
                  yang ramah, serta UMKM yang melimpah,
                  Gentan & Purbayan memberikan
                  kenyamanan karena karakter dan
                  suasananya yang sangat “Family Oriented”.</p>
            </div>
            <div class="md:p-6 p-4 md:w-1/2 mb-2" data-aos="fade-up">
               <a href="<?php echo do_shortcode("[template_dir image='lokasi/lokasi-3.png']"); ?>" data-lightbox="lokasiandlingkungan" data-title="">
                  <div class="rounded overflow-hidden">
                     <img alt="Foto Attaqi Sharia Townhouse Tidak perlu jauh-jauh belanja kebutuhan, supermarket Luwes juga ada" class="object-cover object-center h-full w-full" src="<?php echo do_shortcode("[template_dir image='lokasi/lokasi-3-thumb-min.jpg']"); ?>">
                  </div>
               </a>
               <p class="text-base leading-relaxed mt-2 text-justify">Tidak perlu jauh-jauh belanja kebutuhan,
               supermarket Luwes juga ada, kemajuan di
               Gentan mendorong retail-retail besar untuk
               mendirikan cabang mereka di Gentan.</p>
            </div>
            <div class="md:p-6 p-4 md:w-1/2 mb-2" data-aos="fade-up">
               <a href="<?php echo do_shortcode("[template_dir image='lokasi/LOKASI 4.png']"); ?>" data-lightbox="lokasiandlingkungan" data-title="">
                  <div class="rounded overflow-hidden">
                     <img alt="Foto Attaqi Sharia Townhouse Kenyamanan suasana residensial dan kemudahan ke fasilitas transportasi" class="object-cover object-center h-full w-full" src="<?php echo do_shortcode("[template_dir image='lokasi/lokasi-4-thumb-min.jpg']"); ?>">
                  </div>
               </a>
               <p class="text-base leading-relaxed mt-2 text-justify">Kenyamanan suasana residensial dan
               kemudahan ke fasilitas transportasi, jalan
               tol dan infrastruktur lainnya bisa didapatkan
               semua di Gentan – Purbayan, tanpa harus
               mengorbankan salah satunya.</p>
            </div>
            <div class="md:p-6 p-4 md:w-1/2 mb-2" data-aos="fade-up">
               <a href="<?php echo do_shortcode("[template_dir image='lokasi/LOKASI 5.png']"); ?>" data-lightbox="lokasiandlingkungan" data-title="">
                  <div class="rounded overflow-hidden">
                     <img alt="Foto Attaqi Sharia Townhouse  Sebelas Maret (UNS), Universitas Muhammadiyah Surakarta (UMS)" class="object-cover object-center h-full w-full" src="<?php echo do_shortcode("[template_dir image='lokasi/lokasi-5-thumb-min.jpg']"); ?>">
                  </div>
               </a>
               <p class="text-base leading-relaxed mt-2 text-justify">Fasilitas pendidikan negeri & swasta yang
               mudah dijangkau, mulai dari SD-SMA dan PT
               seperti Universitas Sebelas Maret (UNS),
               Universitas Muhammadiyah Surakarta
               (UMS).</p>
            </div>
            <div class="md:p-6 p-4 md:w-1/2 mb-2" data-aos="fade-up">
               <a href="<?php echo do_shortcode("[template_dir image='lokasi/LOKASI 6.png']"); ?>" data-lightbox="lokasiandlingkungan" data-title="">
                  <div class="rounded overflow-hidden">
                     <img alt="Foto Attaqi Sharia Townhouse Fasilitas umum dan sosial di area Gentan sudah lebih dari cukup" class="object-cover object-center h-full w-full" src="<?php echo do_shortcode("[template_dir image='lokasi/lokasi-6-thumb-min.jpg']"); ?>">
                  </div>
               </a>

               <p class="text-base leading-relaxed mt-2 text-justify">Fasilitas umum dan sosial di area Gentan
               sudah lebih dari cukup untuk memenuhi
               kebutuhan sehari-hari, ditambah jangkauan
               ke berbagai Lifestyle Mall yang dekat dan
               mudah.</p>
            </div>
         </div>
      </div>
   </section>
   <section class="text-white body-font bg-blue-550" id="materialgradeterbaik">
      <div class="container md:px-16 px-10 py-24 mx-auto">
         <div class="flex flex-col">
            <div class="h-1 bg-gray-200 rounded overflow-hidden">
               <div class="w-24 h-full bg-yellow-500"></div>
            </div>
            <div class="flex flex-wrap sm:flex-row flex-col py-6 mb-12 justify-between items-center">
               <h1 class="sm:w-2/5 text-white font-extrabold title-font text-5xl mb-10 md:text-left text-center">MATERIAL ISTIMEWA<br>& GRADE TERBAIK
               </h1>
               <a href="https://www.instagram.com/attaqi_land/" target="_blank" rel="nofollow"
                  class="call-attaqi-btn inline-flex text-white border-0 py-2 px-6 align-middle focus:outline-none bg-yellow-500 hover:text-yellow-500 hover:border-2 hover:border-yellow-500 hover:bg-white hover:text-bold hover:border-4 rounded-full text-lg ml-2">
                  Hubungi @attaqi_land&nbsp<svg class="ig-icon-svg fill-current text-white mt-1"
                     xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24">
                     <path
                        d="M12 2.163c3.204 0 3.584.012 4.85.07 3.252.148 4.771 1.691 4.919 4.919.058 1.265.069 1.645.069 4.849 0 3.205-.012 3.584-.069 4.849-.149 3.225-1.664 4.771-4.919 4.919-1.266.058-1.644.07-4.85.07-3.204 0-3.584-.012-4.849-.07-3.26-.149-4.771-1.699-4.919-4.92-.058-1.265-.07-1.644-.07-4.849 0-3.204.013-3.583.07-4.849.149-3.227 1.664-4.771 4.919-4.919 1.266-.057 1.645-.069 4.849-.069zm0-2.163c-3.259 0-3.667.014-4.947.072-4.358.2-6.78 2.618-6.98 6.98-.059 1.281-.073 1.689-.073 4.948 0 3.259.014 3.668.072 4.948.2 4.358 2.618 6.78 6.98 6.98 1.281.058 1.689.072 4.948.072 3.259 0 3.668-.014 4.948-.072 4.354-.2 6.782-2.618 6.979-6.98.059-1.28.073-1.689.073-4.948 0-3.259-.014-3.667-.072-4.947-.196-4.354-2.617-6.78-6.979-6.98-1.281-.059-1.69-.073-4.949-.073zm0 5.838c-3.403 0-6.162 2.759-6.162 6.162s2.759 6.163 6.162 6.163 6.162-2.759 6.162-6.163c0-3.403-2.759-6.162-6.162-6.162zm0 10.162c-2.209 0-4-1.79-4-4 0-2.209 1.791-4 4-4s4 1.791 4 4c0 2.21-1.791 4-4 4zm6.406-11.845c-.796 0-1.441.645-1.441 1.44s.645 1.44 1.441 1.44c.795 0 1.439-.645 1.439-1.44s-.644-1.44-1.439-1.44z" />
                  </svg></a>
            </div>
         </div>
         <div class="flex flex-wrap sm:-m-4 -mx-4 -mb-10 -mt-4">
            <div class="md:p-6 md:w-1/2 mb-10" data-aos="fade-up">
               <a href="<?php echo do_shortcode("[template_dir image='material/material-1.png']"); ?>" data-lightbox="materialistimewa" data-title="">
                  <div class="rounded overflow-hidden">
                     <img alt="Foto Attaqi Sharia Townhouse Conwood first grade, memberikan suasana kayu semi tropis" class="object-cover object-scale-down h-full w-full" src="<?php echo do_shortcode("[template_dir image='material/material-1-thumb-min.jpg']"); ?>">
                  </div>
               </a>

               <p class="text-base leading-relaxed mt-2 text-justify">Conwood first grade, memberikan suasana
               kayu semi tropis, namun tetap awet karena
               dibuat untuk penggunaan outdoor dan
               tahan terhadap cuaca ekstrim.</p>
            </div>
            <div class="md:p-6 md:w-1/2 mb-10" data-aos="fade-up">
               <a href="<?php echo do_shortcode("[template_dir image='material/material-2.png']"); ?>" data-lightbox="materialistimewa" data-title="">
                  <div class="rounded overflow-hidden">
                     <img alt="Foto Attaqi Sharia Townhouse Flat Ceramic Roof, instalasi unik hingga samping bangunan" class="object-cover object-scale-down h-full w-full" src="<?php echo do_shortcode("[template_dir image='material/material-2-thumb-min.jpg']"); ?>">
                  </div>
               </a>
               <p class="text-base leading-relaxed mt-2 text-justify">Flat Ceramic Roof, instalasi unik hingga
               samping bangunan, memperkuat kesan
               gagah dan elegan, serta menghasilkan tone
               warna bangunan yang pas.</p>
            </div>
            <div class="md:p-6 md:w-1/2 mb-10" data-aos="fade-up">
               <a href="<?php echo do_shortcode("[template_dir image='material/material-3.png']"); ?>" data-lightbox="materialistimewa" data-title="">
                  <div class="rounded overflow-hidden">
                     <img alt="Foto Attaqi Sharia Townhouse Batu alam asli dari Bali, menjadikan nilai seni bangunan semakin berkelas" class="object-cover object-scale-down h-full w-full" src="<?php echo do_shortcode("[template_dir image='material/material-3-thumb-min.jpg']"); ?>">
                  </div>
               </a>
               <p class="text-base leading-relaxed mt-2 text-justify">Batu alam asli dari Bali, menjadikan nilai seni
               bangunan semakin berkelas dan memiliki
               ketahanan yang lebih tinggi dibanding batu
               alam rata-rata.</p>
            </div>
            <div class="md:p-6 md:w-1/2 mb-10" data-aos="fade-up">
               <a href="<?php echo do_shortcode("[template_dir image='material/material-4.png']"); ?>" data-lightbox="materialistimewa" data-title="">
                  <div class="rounded overflow-hidden">
                     <img alt="Foto Attaqi Sharia Townhouse Lantai Granit, memberi kesan lebih mewah" class="object-cover object-scale-down h-full w-full" src="<?php echo do_shortcode("[template_dir image='material/material-4-thumb-min.jpg']"); ?>">
                  </div>
               </a>
               <p class="text-base leading-relaxed mt-2 text-justify">Lantai Granit, memberi kesan lebih mewah,
               warna granit dipilih untuk bisa
               menghidupkan suasana yang terang dan
               menyesuaikan tone warna bagian dalam.</p>
            </div>
            <!-- <div class="md:p-10 md:w-full sm:mb-0 mb-6" data-aos="fade-up">
               <a href="<?php //echo do_shortcode("[template_dir image='denah/DENAH BASIC.png']"); ?>material/material-1.png" data-lightbox="materialistimewa" data-title="">
                  <div class="rounded-lg h-64 overflow-hidden">
                     <img alt="content" class="object-cover object-center h-full w-full"
                        src="<?php //echo do_shortcode("[template_dir image='denah/DENAH BASIC.png']"); ?>material/material-1-thumb.jpg">
                  </div>
               </a>
               <p class="text-base leading-relaxed mt-2 text-justify">Semua material menggunakan grade terbaik dikelasnya dan brand
                  ternama yang sudah teruji
                  secara kualitas hingga layanannya, realisasi dan pembangunan diawasi dengan ketat dan
                  menerapkan standar prosedur mulai dari tes tanah hingga finishing.</p>
            </div> -->
         </div>
      </div>
   </section>
   <section class="text-gray-600 body-font" id="fasilitas">
      <div class="container md:px-16 px-10 py-24 mx-auto">
         <div class="flex flex-col">
            <div class="h-1 bg-gray-200 rounded overflow-hidden">
               <div class="w-24 h-full bg-yellow-500"></div>
            </div>
            <div class="flex flex-wrap sm:flex-row flex-col py-6 mb-12 justify-between items-center">
               <h1 class="sm:w-2/5 text-gray-900 font-extrabold title-font text-5xl mb-10 text-center md:text-left">FASILITAS KHUSUS
               </h1>
               <a href="https://www.instagram.com/attaqi_land/" target="_blank" rel="nofollow"
                  class="call-attaqi-btn inline-flex text-white border-0 py-2 px-6 align-middle focus:outline-none bg-yellow-500 hover:text-yellow-500 hover:border-2 hover:border-yellow-500 hover:bg-white hover:text-bold hover:border-4 border rounded-full text-lg ml-2">
                  Hubungi @attaqi_land&nbsp<svg class="ig-icon-svg fill-current text-white mt-1"
                     xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24">
                     <path
                        d="M12 2.163c3.204 0 3.584.012 4.85.07 3.252.148 4.771 1.691 4.919 4.919.058 1.265.069 1.645.069 4.849 0 3.205-.012 3.584-.069 4.849-.149 3.225-1.664 4.771-4.919 4.919-1.266.058-1.644.07-4.85.07-3.204 0-3.584-.012-4.849-.07-3.26-.149-4.771-1.699-4.919-4.92-.058-1.265-.07-1.644-.07-4.849 0-3.204.013-3.583.07-4.849.149-3.227 1.664-4.771 4.919-4.919 1.266-.057 1.645-.069 4.849-.069zm0-2.163c-3.259 0-3.667.014-4.947.072-4.358.2-6.78 2.618-6.98 6.98-.059 1.281-.073 1.689-.073 4.948 0 3.259.014 3.668.072 4.948.2 4.358 2.618 6.78 6.98 6.98 1.281.058 1.689.072 4.948.072 3.259 0 3.668-.014 4.948-.072 4.354-.2 6.782-2.618 6.979-6.98.059-1.28.073-1.689.073-4.948 0-3.259-.014-3.667-.072-4.947-.196-4.354-2.617-6.78-6.979-6.98-1.281-.059-1.69-.073-4.949-.073zm0 5.838c-3.403 0-6.162 2.759-6.162 6.162s2.759 6.163 6.162 6.163 6.162-2.759 6.162-6.163c0-3.403-2.759-6.162-6.162-6.162zm0 10.162c-2.209 0-4-1.79-4-4 0-2.209 1.791-4 4-4s4 1.791 4 4c0 2.21-1.791 4-4 4zm6.406-11.845c-.796 0-1.441.645-1.441 1.44s.645 1.44 1.441 1.44c.795 0 1.439-.645 1.439-1.44s-.644-1.44-1.439-1.44z" />
                  </svg></a>
            </div>
         </div>
         <div class="flex flex-wrap sm:-m-4 -mx-4 -mb-10 -mt-4">
            <div class="md:p-5 md:w-1/3 sm:mb-0 mb-6 w-full" data-aos="fade-up">
               <a href="<?php echo do_shortcode("[template_dir image='fasilitas/fasilitas-1.png']"); ?>" data-lightbox="fasilitas" data-title="Musholla">
                  <div class="rounded overflow-hidden">
                     <img alt="Foto Attaqi Sharia Townhouse Musholla" class="object-scale-down object-center h-full w-full" src="<?php echo do_shortcode("[template_dir image='fasilitas/fasilitas-1-thumb-min.jpg']"); ?>">
                  </div>
               </a>
               <p class="text-base leading-relaxed mt-2 text-center">Musholla.</p>
            </div>
            <div class="md:p-5 md:w-1/3 sm:mb-0 mb-6 w-full" data-aos="fade-up">
               <a href="<?php echo do_shortcode("[template_dir image='fasilitas/fasilitas-2.png']"); ?>" data-lightbox="fasilitas" data-title="Playground">
                  <div class="rounded overflow-hidden">
                     <img alt="Foto Attaqi Sharia Townhouse Playground" class="object-scale-down object-center h-full w-full" src="<?php echo do_shortcode("[template_dir image='fasilitas/fasilitas-2-thumb-min.jpg']"); ?>">
                  </div>
               </a>
               <p class="text-base leading-relaxed mt-2 text-center">Playground.</p>
            </div>
            <div class="md:p-5 md:w-1/3 sm:mb-0 mb-6 w-full" data-aos="fade-up">
               <a href="<?php echo do_shortcode("[template_dir image='fasilitas/fasilitas-3.png']"); ?>" data-lightbox="fasilitas" data-title="Komunal Warga">
                  <div class="rounded overflow-hidden">
                     <img alt="Foto Attaqi Sharia Townhouse Komunal Warga" class="object-scale-down object-center h-full w-full" src="<?php echo do_shortcode("[template_dir image='fasilitas/fasilitas-3-thumb-min.jpg']"); ?>">
                  </div>
               </a>
               <p class="text-base leading-relaxed mt-2 text-center">Komunal Warga.</p>
            </div>
            <div class="md:p-5 md:w-1/3 sm:mb-0 mb-6 w-full" data-aos="fade-up">
               <a href="<?php echo do_shortcode("[template_dir image='fasilitas/fasilitas-4.png']"); ?>" data-lightbox="fasilitas" data-title="Smarthome terpasang untuk semua rumah">
               <div class="rounded overflow-hidden">
                  <img alt="Foto Attaqi Sharia Townhouse Smarthome terpasang untuk semua rumah" class="object-scale-down object-center h-full w-full" src="<?php echo do_shortcode("[template_dir image='fasilitas/fasilitas-4-thumb-min.jpg']"); ?>">
               </div>
               </a>
               <p class="text-base leading-relaxed mt-2 text-center">Smarthome terpasang untuk semua rumah.</p>
            </div>
            <div class="md:p-5 md:w-1/3 sm:mb-0 mb-6 w-full" data-aos="fade-up">
               <a href="<?php echo do_shortcode("[template_dir image='fasilitas/fasilitas-5.png']"); ?>" data-lightbox="fasilitas" data-title="CCTV Lingkungan">
               <div class="rounded overflow-hidden">
                  <img alt="Foto Attaqi Sharia Townhouse CCTV Lingkungan" class="object-scale-down object-center h-full w-full" src="<?php echo do_shortcode("[template_dir image='fasilitas/fasilitas-5-thumb-min.jpg']"); ?>">
               </div>
               </a>
               <p class="text-base leading-relaxed mt-2 text-center">CCTV Lingkungan.</p>
            </div>
            <div class="md:p-5 md:w-1/3 sm:mb-0 mb-6 w-full" data-aos="fade-up">
               <a href="<?php echo do_shortcode("[template_dir image='fasilitas/fasilitas-6.png']"); ?>" data-lightbox="fasilitas" data-title="One Gate System">
               <div class="rounded overflow-hidden">
                  <img alt="Foto Attaqi Sharia Townhouse One Gate System" class="object-scale-down object-center h-full w-full" src="<?php echo do_shortcode("[template_dir image='fasilitas/fasilitas-6-thumb-min.jpg']"); ?>">
               </div>
               </a>
               <p class="text-base leading-relaxed mt-2 text-center">One Gate System.</p>
            </div>
         </div>
      </div>
   </section>
   <section class="text-white body-font bg-blue-550" id="fullyfurnishedservice">
      <div class="container md:px-16 px-10 py-24 mx-auto">
         <div class="flex flex-col">
            <div class="h-1 bg-gray-200 rounded overflow-hidden">
               <div class="w-24 h-full bg-yellow-500"></div>
            </div>
            <div class="flex flex-wrap sm:flex-row flex-col py-6 mb-12 justify-between items-center">
               <h1 class="sm:w-2/5 text-white font-extrabold title-font text-5xl mb-10 md:text-left text-center">ATTAQI FULLY FURNISHED SERVICE
               </h1>
               <a href="https://www.instagram.com/attaqi_land/" target="_blank" rel="nofollow"
                  class="call-attaqi-btn inline-flex text-white border-0 py-2 px-6 align-middle focus:outline-none bg-yellow-500 hover:text-yellow-500 hover:border-2 hover:border-yellow-500 hover:bg-white hover:text-bold hover:border-4 rounded-full text-lg ml-2">
                  Hubungi @attaqi_land&nbsp<svg class="ig-icon-svg fill-current text-white mt-1"
                     xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24">
                     <path
                        d="M12 2.163c3.204 0 3.584.012 4.85.07 3.252.148 4.771 1.691 4.919 4.919.058 1.265.069 1.645.069 4.849 0 3.205-.012 3.584-.069 4.849-.149 3.225-1.664 4.771-4.919 4.919-1.266.058-1.644.07-4.85.07-3.204 0-3.584-.012-4.849-.07-3.26-.149-4.771-1.699-4.919-4.92-.058-1.265-.07-1.644-.07-4.849 0-3.204.013-3.583.07-4.849.149-3.227 1.664-4.771 4.919-4.919 1.266-.057 1.645-.069 4.849-.069zm0-2.163c-3.259 0-3.667.014-4.947.072-4.358.2-6.78 2.618-6.98 6.98-.059 1.281-.073 1.689-.073 4.948 0 3.259.014 3.668.072 4.948.2 4.358 2.618 6.78 6.98 6.98 1.281.058 1.689.072 4.948.072 3.259 0 3.668-.014 4.948-.072 4.354-.2 6.782-2.618 6.979-6.98.059-1.28.073-1.689.073-4.948 0-3.259-.014-3.667-.072-4.947-.196-4.354-2.617-6.78-6.979-6.98-1.281-.059-1.69-.073-4.949-.073zm0 5.838c-3.403 0-6.162 2.759-6.162 6.162s2.759 6.163 6.162 6.163 6.162-2.759 6.162-6.163c0-3.403-2.759-6.162-6.162-6.162zm0 10.162c-2.209 0-4-1.79-4-4 0-2.209 1.791-4 4-4s4 1.791 4 4c0 2.21-1.791 4-4 4zm6.406-11.845c-.796 0-1.441.645-1.441 1.44s.645 1.44 1.441 1.44c.795 0 1.439-.645 1.439-1.44s-.644-1.44-1.439-1.44z" />
                  </svg></a>
            </div>
         </div>
         <div class="flex flex-wrap lg:flex-row flex-col sm:-m-4 -mx-4 -mb-10 -mt-4">
            <div class="lg:p-0 lg:w-1/2 w-full sm:mb-0 mb-6" data-aos="fade-up">
               <div class="md:mb-36 mb-28">
                  <div class="swiper-container gallery-top2 z-0 relative md:w-80">
                     <div class="swiper-wrapper">
                        <div class="swiper-slide">
                           <div class="swiper-slide-container">
                              <img class="object-cover object-center rounded" alt="Foto Attaqi Sharia Townhouse Furnished" src="<?php echo do_shortcode("[template_dir image='fully-furnished/ff1-min.jpg']"); ?>">
                           </div>
                        </div>
                        <div class="swiper-slide">
                           <div class="swiper-slide-container">
                              <img class="object-cover object-center rounded" alt="Foto Attaqi Sharia Townhouse Furnished" src="<?php echo do_shortcode("[template_dir image='fully-furnished/ff2-min.jpg']"); ?>">
                           </div>
                        </div>
                        <div class="swiper-slide">
                           <div class="swiper-slide-container">
                              <img class="object-cover object-center rounded" alt="Foto Attaqi Sharia Townhouse Furnished" src="<?php echo do_shortcode("[template_dir image='fully-furnished/ff3-min.jpg']"); ?>">
                           </div>
                        </div>
                        <div class="swiper-slide">
                           <div class="swiper-slide-container">
                              <img class="object-cover object-center rounded" alt="Foto Attaqi Sharia Townhouse Furnished" src="<?php echo do_shortcode("[template_dir image='fully-furnished/ff4-min.jpg']"); ?>">
                           </div>
                        </div>
                        <div class="swiper-slide">
                           <div class="swiper-slide-container">
                              <img class="object-cover object-center rounded" alt="Foto Attaqi Sharia Townhouse Furnished" src="<?php echo do_shortcode("[template_dir image='fully-furnished/ff5-min.jpg']"); ?>">
                           </div>
                        </div>
                     </div>
                     <!-- Add Arrows -->
                     <div class="swiper-button-next swiper-button-next_slideshow<?=$module?>"></div>
                     <div class="swiper-button-prev swiper-button-prev_slideshow<?=$module?>"></div>
                  </div>
                  <div class="swiper-container gallery-thumbs2 z-10 absolute md:w-90 w-80">
                     <div class="swiper-wrapper">
                        <div class="swiper-slide">
                           <div class="swiper-slide-container">
                              <img class="object-cover object-center rounded-lg" alt="Foto Attaqi Sharia Townhouse Furnished" src="<?php echo do_shortcode("[template_dir image='fully-furnished/ff1-min.jpg']"); ?>">
                           </div>
                        </div>
                        <div class="swiper-slide">
                           <div class="swiper-slide-container">
                              <img class="object-cover object-center rounded" alt="Foto Attaqi Sharia Townhouse Furnished" src="<?php echo do_shortcode("[template_dir image='fully-furnished/ff2-min.jpg']"); ?>">
                           </div>
                        </div>
                        <div class="swiper-slide">
                           <div class="swiper-slide-container">
                              <img class="object-cover object-center rounded" alt="Foto Attaqi Sharia Townhouse Furnished" src="<?php echo do_shortcode("[template_dir image='fully-furnished/ff3-min.jpg']"); ?>">
                           </div>
                        </div>
                        <div class="swiper-slide">
                           <div class="swiper-slide-container">
                              <img class="object-cover object-center rounded" alt="Foto Attaqi Sharia Townhouse Furnished" src="<?php echo do_shortcode("[template_dir image='fully-furnished/ff4-min.jpg']"); ?>">
                           </div>
                        </div>
                        <div class="swiper-slide">
                           <div class="swiper-slide-container">
                              <img class="object-cover object-center rounded" alt="Foto Attaqi Sharia Townhouse Furnished" src="<?php echo do_shortcode("[template_dir image='fully-furnished/ff5-min.jpg']"); ?>">
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="flex justify-around md:flex-row flex-col">
                  <a href="https://www.instagram.com/attaqi_land/" target="_blank" rel="nofollow" target="_blank"
                     class="call-attaqi-btn text-white border-0 py-2 px-6 align-middle focus:outline-none bg-yellow-500 hover:text-yellow-500 hover:border-2 hover:border-yellow-500 hover:bg-white hover:text-bold hover:border-4 rounded-full text-lg text-center">
                     Hubungi Marketing
                  </a>

                  <a href="https://attaqiland.com/pricelist" target="_blank"
                     class="call-attaqi-btn text-white border-0 py-2 px-6 align-middle focus:outline-none bg-yellow-500 hover:text-yellow-500 hover:border-2 hover:border-yellow-500 hover:bg-white hover:text-bold hover:border-4 rounded-full text-lg sm:mt-0 mt-4 text-center">
                     Download Brosur</a>
               </div>
            </div>
            <div class="md:p-10 lg:w-1/2 w-full sm:mb-0 mb-6 aos-init aos-animate" data-aos="fade-up">
               <h3 class="text-base leading-relaxed font-semibold sm:text-justify text-center text-3xl">ATTAQI FULLY FURNISHED SERVICE</h3>
               <p class="text-base leading-relaxed mt-2 text-justify">
                  Adalah layanan untuk para pembeli rumah
                  dan penghuni, yang mau mengisi rumahnya
                  dengan perabotan, mulai dari per ruangan
                  atau secara keseluruhan (fully furnished)
               </p>
               <p class="text-base leading-relaxed mt-2 text-justify">
                  ATTAQI LAND Bekerjasama dengan interior
                  designer dan retail, untuk memilih
                  perbabotan juga melalui tahap desain
                  supaya mendapatkan perabot atau furnitur
                  yang sesuai tema, tone warna dan tata
                  letak. Setelah melalui tahap diskusi dengan
                  penghuni / pemilik rumah, ATTAQI LAND
                  akan melakukan pengadaan dan penataan
               </p>
               <p class="text-base leading-relaxed mt-2 text-justify">
                  Layanan ini eksklusif diberikan bagi para
                  penghuni atau pemilik yang berminat.
               </p>
            </div>
         </div>
      </div>
   </section>
   <section class="text-gray-600 body-font" id="pelayanan">
      <div class="container md:px-16 px-10 py-24 mx-auto">
         <div class="flex flex-col">
            <div class="h-1 bg-gray-200 rounded overflow-hidden">
               <div class="w-24 h-full bg-yellow-500"></div>
            </div>
            <div class="flex flex-wrap sm:flex-row flex-col py-6 mb-6 justify-between items-center">
               <h1 class="sm:w-2/5 text-gray-900 font-extrabold title-font text-5xl mb-2 sm:mb-0 mb-10">LAYANAN
               </h1>
               <a href="https://www.instagram.com/attaqi_land/" target="_blank" rel="nofollow"
                  class="call-attaqi-btn inline-flex text-white border-0 py-2 px-6 align-middle focus:outline-none bg-yellow-500 hover:text-yellow-500 hover:border-2 hover:border-yellow-500 hover:bg-white hover:text-bold hover:border-4 border rounded-full text-lg ml-2">
                  Hubungi @attaqi_land&nbsp<svg class="ig-icon-svg fill-current text-white mt-1"
                     xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24">
                     <path
                        d="M12 2.163c3.204 0 3.584.012 4.85.07 3.252.148 4.771 1.691 4.919 4.919.058 1.265.069 1.645.069 4.849 0 3.205-.012 3.584-.069 4.849-.149 3.225-1.664 4.771-4.919 4.919-1.266.058-1.644.07-4.85.07-3.204 0-3.584-.012-4.849-.07-3.26-.149-4.771-1.699-4.919-4.92-.058-1.265-.07-1.644-.07-4.849 0-3.204.013-3.583.07-4.849.149-3.227 1.664-4.771 4.919-4.919 1.266-.057 1.645-.069 4.849-.069zm0-2.163c-3.259 0-3.667.014-4.947.072-4.358.2-6.78 2.618-6.98 6.98-.059 1.281-.073 1.689-.073 4.948 0 3.259.014 3.668.072 4.948.2 4.358 2.618 6.78 6.98 6.98 1.281.058 1.689.072 4.948.072 3.259 0 3.668-.014 4.948-.072 4.354-.2 6.782-2.618 6.979-6.98.059-1.28.073-1.689.073-4.948 0-3.259-.014-3.667-.072-4.947-.196-4.354-2.617-6.78-6.979-6.98-1.281-.059-1.69-.073-4.949-.073zm0 5.838c-3.403 0-6.162 2.759-6.162 6.162s2.759 6.163 6.162 6.163 6.162-2.759 6.162-6.163c0-3.403-2.759-6.162-6.162-6.162zm0 10.162c-2.209 0-4-1.79-4-4 0-2.209 1.791-4 4-4s4 1.791 4 4c0 2.21-1.791 4-4 4zm6.406-11.845c-.796 0-1.441.645-1.441 1.44s.645 1.44 1.441 1.44c.795 0 1.439-.645 1.439-1.44s-.644-1.44-1.439-1.44z" />
                  </svg></a>
            </div>
         </div>
         <div class="flex flex-col -mb-10 -mt-4">
            <div class="flex md:flex-row flex-col py-4 sm:mb-0 mb-2">
               <div class="rounded overflow-hidden">
                  <a href="<?php echo do_shortcode("[template_dir image='layanan/layanan-1.png']"); ?>" data-lightbox="layanan" data-title="">
                     <img alt="Foto Sharia Townhouse ATTAQI LAND berusaha untu selalu siap melayani" class="object-scale-down object-center h-full w-full" src="<?php echo do_shortcode("[template_dir image='layanan/layanan-1-min.png']"); ?>">
                  </a>
                  </div>
               <div class="flex flex-col justify-center content-center md:w-1/2 w-full lg:lx-20 sm:pl-20 sm:mt-0 mt-4">
               <h3 class="text-xl font-semibold md:text-left text-center">AFTER SALES</h3>
               <p class="text-base leading-relaxed mt-2 text-justify">
                  Demi selalu menjalin hubungan baik, ATTAQI
                  LAND berusaha untu selalu siap melayani
                  penghuni atau pemilik rumah, jika terjadi hal-hal
                  yang tidak diinginkan.
               </p>
               </div>
            </div>
            <div class="flex md:flex-row flex-col py-4 sm:mb-0 mb-2">
               <div class="rounded overflow-hidden">
                  <a href="<?php echo do_shortcode("[template_dir image='layanan/layanan-2.png']"); ?>" data-lightbox="layanan" data-title="">
                     <img alt="Foto ATTAQI LAND memberikan pelayanan jika ada request perubahan desain minor" class="object-scale-down object-center h-full w-full" src="<?php echo do_shortcode("[template_dir image='layanan/layanan-2-min.png']"); ?>">
                  </a>
                  </div>
               <div class="flex flex-col justify-center content-center md:w-1/2 w-full lg:lx-20 sm:pl-20 sm:mt-0 mt-4">
                  <h3 class="text-xl font-semibold md:text-left text-center">DESIGN REQUEST</h3>
                  <p class="text-base leading-relaxed mt-2 text-justify">
                     Kebutuhan setiap penghuni atau pemilik rumah
                     tentu berbeda beda, ATTAQI LAND memberikan
                     pelayanan jika ada request perubahan desain
                     minor, tata letak atau penambahan fasilitas
                     Kami akan melakukan studi dan memberikan
                     penawaran ke calon pembeli
                  </p>
               </div>
            </div>
            <div class="flex flex-col">
                  <p class="text-center w-full text-white border-0 py-2 px-6 focus:outline-none bg-yellow-500 hover:text-yellow-500 hover:border-2 hover:border-yellow-500 hover:bg-white hover:text-bold hover:border-4 border rounded-full text-lg mr-2 font-semibold">Lihat Berikut contoh request pembeli yang sudah kami studi dan realisasikan</p>
                  <div class="flex md:flex-row flex-col">
                     <div class="md:w-1/5 md:p-2 py-2 w-full">
                        <a href="<?php echo do_shortcode("[template_dir image='request/request-1.png']"); ?>" data-lightbox="request" data-title="">
                           <img alt="Foto Attaqi Sharia Townhouse " class="object-scale-down object-center h-full w-full rounded" src="<?php echo do_shortcode("[template_dir image='request/request-1-min.jpg']"); ?>">
                        </a>
                     </div>
                     <div class="md:w-1/5 md:p-2 py-2 w-full">
                        <a href="<?php echo do_shortcode("[template_dir image='request/request-2.png']"); ?>" data-lightbox="request" data-title="">
                           <img alt="Foto Attaqi Sharia Townhouse " class="object-scale-down object-center h-full w-full rounded" src="<?php echo do_shortcode("[template_dir image='request/request-2-min.jpg']"); ?>">
                        </a>
                     </div>
                     <div class="md:w-1/5 md:p-2 py-2 w-full">
                        <a href="<?php echo do_shortcode("[template_dir image='request/request-3.png']"); ?>" data-lightbox="request" data-title="">
                           <img alt="Foto Attaqi Sharia Townhouse " class="object-scale-down object-center h-full w-full rounded" src="<?php echo do_shortcode("[template_dir image='request/request-3-min.jpg']"); ?>">
                        </a>
                     </div>
                     <div class="md:w-1/5 md:p-2 py-2 w-full">
                        <a href="<?php echo do_shortcode("[template_dir image='request/request-4.png']"); ?>" data-lightbox="request" data-title="">
                           <img alt="Foto Attaqi Sharia Townhouse " class="object-scale-down object-center h-full w-full rounded" src="<?php echo do_shortcode("[template_dir image='request/request-4-min.jpg']"); ?>">
                        </a>
                     </div>
                     <div class="md:w-1/5 md:p-2 py-2 w-full">
                        <a href="<?php echo do_shortcode("[template_dir image='request/request-5.png']"); ?>" data-lightbox="request" data-title="">
                           <img alt="Foto Attaqi Sharia Townhouse " class="object-scale-down object-center h-full w-full rounded" src="<?php echo do_shortcode("[template_dir image='request/request-5-min.jpg']"); ?>">
                        </a>
                     </div>
                  </div>
            </div>
            <div class="flex md:flex-row flex-col py-4 sm:mb-0 mb-2">
               <div class="rounded overflow-hidden">
                  <a href="<?php echo do_shortcode("[template_dir image='layanan/layanan-3.png']"); ?>" data-lightbox="layanan" data-title="">
                     <img alt="Foto Attaqi Sharia Townhouse ATTAQI LAND memiliki layanan realisasi kepemilikan rumah dengan skema kredit syariah" class="object-scale-down object-center h-full w-full" src="<?php echo do_shortcode("[template_dir image='layanan/layanan-3-min.png']"); ?>">
                  </a>
                  </div>
               <div class="flex flex-col justify-center content-center md:w-1/2 w-full lg:lx-20 sm:pl-20 sm:mt-0 mt-4">
               <h3 class="text-xl font-semibold md:text-left text-center">DESIGN & BUILD</h3>
               <p class="text-base leading-relaxed mt-2 text-justify">
                  ATTAQI LAND memiliki layanan realisasi
                  kepemilikan rumah dengan skema kredit
                  syariah, jika lokasi Attaqi Luxury Sharia
                  Townhouse dirasa kurang tepat bagi anda. Kami
                  bisa membantu realisasi rumah di lokasi yang
                  anda inginkan, mulai dari pencarian lahan
                  desain, perijinan hingga pembangunan
               </p>
               </div>
            </div>
            <div class="flex md:flex-row flex-col py-4 sm:mb-0 mb-2">
               <div class="rounded overflow-hidden">
                  <a href="<?php echo do_shortcode("[template_dir image='layanan/layanan-4-01.png']"); ?>" data-lightbox="layanan" data-title="">
                     <img alt="Foto Attaqi Sharia Townhouse Memberikan layanan berupa pemilihan dan simulasi perabotan / furnitur" class="object-scale-down object-center h-full w-full" src="<?php echo do_shortcode("[template_dir image='layanan/layanan-4-01-min.png']"); ?>">
                  </a>
                  </div>
               <div class="flex flex-col justify-center content-center md:w-1/2 w-full lg:lx-20 sm:pl-20 sm:mt-0 mt-4">
               <h3 class="text-xl font-semibold md:text-left text-center">PENGADAAN PERABOTAN RUMAH TANGGA</h3>
               <p class="text-base leading-relaxed mt-2 text-justify">
                  Memberikan layanan berupa pemilihan dan
                  simulasi perabotan / furnitur, serta pengadaan
                  dan penataan dalam rumah anda
               </p>
               </div>
            </div>
         </div>
      </div>
   </section>
   <section class="text-white body-font bg-blue-550" id="designandtype">
      <div class="container md:px-16 px-10 py-24 mx-auto">
         <div class="flex flex-col">
            <div class="h-1 bg-gray-200 rounded overflow-hidden">
               <div class="w-24 h-full bg-yellow-500"></div>
            </div>
            <div class="flex flex-wrap sm:flex-row flex-col py-6 md:mb-12 mb-0 justify-between items-center">
               <h1 class="sm:w-2/5 text-white font-extrabold title-font text-5xl mb-2 sm:mb-0 md:text-left text-center">SITE PLAN ATTAQI LUXURY SHARIA TOWNHOUSE
               </h1>
            </div>
         </div>
         <div class="w-full md:mb-10 mb-0 object-scale-down object-center">
            <a href="<?php echo do_shortcode("[template_dir image='siteplan/siteplan.png']"); ?>" data-lightbox="siteplan" data-title="">
               <img alt="GAMBAR SITE PLAN ATTAQI LUXURY SHARIA TOWNHOUSE" class="object-scale-down object-center h-full w-full rounded" src="<?php echo do_shortcode("[template_dir image='siteplan/siteplan-min.png']"); ?>">
            </a>
         </div>
      </div>
   </section>
   <section class="text-gray-600 body-font" id="video-preview">
      <div class="container md:px-16 px-10 py-24 mx-auto">
         <div class="flex flex-col">
            <div class="h-1 bg-gray-200 rounded overflow-hidden">
               <div class="w-24 h-full bg-yellow-500"></div>
            </div>
            <div class="flex flex-wrap sm:flex-row flex-col py-6 md:mb-12 mb-0 justify-between items-center">
               <h1 class="sm:w-2/5 text-gray-900 font-extrabold title-font text-5xl mb-2 sm:mb-0 md:text-left text-center">VIDEO TEASER
               </h1>
            </div>
         </div>
         <div class="flex justify-center">
            <iframe class="lg:w-full md:w-full md:w-5/6 w-full md:mb-10 mb-0 object-cover object-center rounded h-96" src="https://www.youtube.com/embed/GUP4ougiefQ?start=8" title="YouTube video player"
               frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
               allowfullscreen></iframe>
         </div>
      </div>
   </section>
   <section class="text-white body-font bg-blue-550" id="faq">
      <div class="container md:px-16 px-10 py-24 mx-auto">
         <div class="flex flex-col">
            <div class="h-1 bg-gray-200 rounded overflow-hidden">
               <div class="w-24 h-full bg-yellow-500"></div>
            </div>
            <div class="flex flex-wrap sm:flex-row flex-col py-6 md:mb-12 mb-0 justify-between items-center">
               <h1 class="sm:w-2/5 text-white font-extrabold title-font text-5xl mb-2 sm:mb-0 md:text-left text-center">FREQUENTLY ASKED QUESTION (FAQ)
               </h1>
            </div>
         </div>
         <div class="flex justify-center">
            <div class="container md:px-5 p-0 mx-auto">
               <div class="flex flex-wrap lg:w-4/5 sm:mx-auto sm:mb-2 -mx-2">
                  <div class="w-full lg:w-full md:px-4 px-0 py-2">
                     <details class="mb-4 cursor-pointer">
                        <summary class="font-semibold border-yellow-500 border-4 rounded-md py-2 px-4 mb-3 cursor-pointer">
                           <span class="cursor-pointer">Kenapa harga CASH dan KREDIT nya beda? Katanya tidak ada bunga?</span>
                        </summary>

                        <span>
                           Itu namanya margin, bukan bunga.
                        </span>
                     </details>
                     <details class="mb-4">
                        <summary class="font-semibold border-yellow-500 border-4 rounded-md py-2 px-4 mb-3">
                           Apa perbedaan margin dengan bunga?
                        </summary>

                        <span>
                           MARGIN adalah keuntungan yang didapat dari jual beli (Halal), sedangkan bunga adalah keuntungan yang didapat dari utang piutang.
                        </span>
                     </details>
                     <details class="mb-4">
                        <summary class="font-semibold border-yellow-500 border-4 rounded-md py-2 px-4 mb-3">
                           Lalu apa perbedaannya dengan Bank?
                        </summary>

                        <span>
                           Bank kebanyakan meminjamkan uang, bukan menjual produk.
                        </span>
                     </details>
                     <details class="mb-4">
                        <summary class="font-semibold border-yellow-500 border-4 rounded-md py-2 px-4 mb-3">
                           Apakah ada biaya KPR seperti Admin, Provisi & Asuransi?
                        </summary>

                        <span>
                           Tidak Ada.
                        </span>
                     </details>
                     <details class="mb-4">
                        <summary class="font-semibold border-yellow-500 border-4 rounded-md py-2 px-4 mb-3">
                           Apakah bisa pilih lokasi sendiri?
                        </summary>

                        <span>
                           Secara umum tidak bisa, karena kita jual produk bukan pembiayaan, tapi di luar hal ini, ATTAQI memiliki jasa design & build bagi kamu yang menginginkan rumah dilokasi lain. ATTAQI membantu kamu cari tanah, desain hingga terbangun.
                        </span>
                     </details>
                     <details class="mb-4">
                        <summary class="font-semibold border-yellow-500 border-4 rounded-md py-2 px-4 mb-3">
                           Apakah Down Payment (DP) bisa dicicil?
                        </summary>

                        <span>
                           Bisa.
                        </span>
                     </details>
                     <details class="mb-4">
                        <summary class="font-semibold border-yellow-500 border-4 rounded-md py-2 px-4 mb-3">
                           Beneran gak ada denda kalau telat bayar?
                        </summary>

                        <span>
                           Tidak ada, denda juga termasuk riba.
                        </span>
                     </details>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </section>
<?php get_footer();?>