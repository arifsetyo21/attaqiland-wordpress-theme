
<?php get_header();?>
   <section class="text-gray-600 bg-blue-550 body-font bg-white">
      <div class="container md:px-16 px-10 py-24 mx-auto">
         <div class="flex flex-col">
            <div class="h-1 bg-gray-500 rounded overflow-hidden">
               <div class="w-24 h-full bg-yellow-500"></div>
            </div>
            <div class="flex flex-wrap sm:flex-row flex-col py-6 mb-12">
               <h1
                  class="sm:w-2/5 text-white font-bold title-font text-4xl mb-2 sm:mb-0 md:text-left text-center md:mb-0 mb-10">
                  Progress Pembangunan</h1>
            </div>
         </div>
         <div class="flex md:flex-row flex-col flex-wrap -m-4">
            <div class="lg:w-full sm:w-full p-4" data-aos="fade-up">
               <div class="flex relative">
                  <img alt="Attaqi Sharia Townhouse Foto Desain Type Basic"
                     class="absolute inset-0 w-full h-full object-scale-down object-center rounded"
                     src="<?php echo do_shortcode("[template_dir image='progres/5_22-mei-2021_paving-jalan-lin.jpg']"); ?>">
                        <a href="<?php echo do_shortcode("[template_dir image='progres/5_22-mei-2021_paving-jalan-lin.jpg']"); ?>" data-lightbox="basicDesign" data-title=""
                     class="px-0 lg:py-68 md:py-32 sm:py-20 py-16 relative z-10 w-full bg-white opacity-0 hover:opacity-100 text-center rounded">
                     <h1 class="title-font text-lg font-medium text-gray-900 mb-3">22 Mei 2021 - Paving Jalan Lingkungan</h1>
                  </a>
               </div>
            </div>
         </div>
         <div class="flex md:flex-row flex-col flex-wrap -m-4">
            <div class="lg:w-full sm:w-full p-4" data-aos="fade-up">
               <div class="flex relative">
                  <img alt="Attaqi Sharia Townhouse Foto Desain Type Basic"
                     class="absolute inset-0 w-full h-full object-scale-down object-center rounded"
                     src="<?php echo do_shortcode("[template_dir image='progres/4_1-des-2020_instalasi-kabel.jpg']"); ?>">
                  <a href="<?php echo do_shortcode("[template_dir image='progres/4_1-des-2020_instalasi-kabel.jpg']"); ?>" data-lightbox="basicDesign" data-title=""
                     class="px-0 lg:py-68 md:py-32 sm:py-20 py-16 relative z-10 w-full bg-white opacity-0 hover:opacity-100 text-center rounded">
                     <h1 class="title-font text-lg font-medium text-gray-900 mb-3">1 Desember 2020 - Instalasi Kabel Rapi & Kokoh</h1>
                  </a>
               </div>
            </div>
         </div>
         <div class="flex md:flex-row flex-col flex-wrap -m-4">
            <div class="lg:w-full sm:w-full p-4" data-aos="fade-up">
               <div class="flex relative">
                  <img alt="Attaqi Sharia Townhouse Foto Desain Type Basic"
                     class="absolute inset-0 w-full h-full object-scale-down object-center rounded"
                     src="<?php echo do_shortcode("[template_dir image='progres/3_20-mei-2020_finishing-kavlin.jpg']"); ?>">
                  <a href="<?php echo do_shortcode("[template_dir image='progres/3_20-mei-2020_finishing-kavlin.jpg']"); ?>" data-lightbox="basicDesign" data-title=""
                     class="px-0 lg:py-68 md:py-32 sm:py-20 py-16 relative z-10 w-full bg-white opacity-0 hover:opacity-100 text-center rounded">
                     <h1 class="title-font text-lg font-medium text-gray-900 mb-3">20 Mei 2020 - Finishing Kavling 7</h1>
                  </a>
               </div>
            </div>
         </div>
         <div class="flex md:flex-row flex-col flex-wrap -m-4">
            <div class="lg:w-full sm:w-full p-4" data-aos="fade-up">
               <div class="flex relative">
                  <img alt="Attaqi Sharia Townhouse Foto Desain Type Basic"
                     class="absolute inset-0 w-full h-full object-scale-down object-center rounded"
                     src="<?php echo do_shortcode("[template_dir image='progres/2_15-feb-2020_finishing-facade.jpg']"); ?>">
                  <a href="<?php echo do_shortcode("[template_dir image='progres/2_15-feb-2020_finishing-facade.jpg']"); ?>" data-lightbox="basicDesign" data-title=""
                     class="px-0 lg:py-68 md:py-32 sm:py-20 py-16 relative z-10 w-full bg-white opacity-0 hover:opacity-100 text-center rounded">
                     <h1 class="title-font text-lg font-medium text-gray-900 mb-3">15 Februari 2020 - Finishing Facade Kavling 7</h1>
                  </a>
               </div>
            </div>
         </div>
         <div class="flex md:flex-row flex-col flex-wrap -m-4">
            <div class="lg:w-full sm:w-full p-4" data-aos="fade-up">
               <div class="flex relative">
                  <img alt="Attaqi Sharia Townhouse Foto Desain Type Basic"
                     class="absolute inset-0 w-full h-full object-scale-down object-center rounded"
                     src="<?php echo do_shortcode("[template_dir image='progres/1_10-feb-2020_finishing-facade.jpg']"); ?>">
                  <a href="<?php echo do_shortcode("[template_dir image='progres/1_10-feb-2020_finishing-facade.jpg']"); ?>" data-lightbox="basicDesign" data-title=""
                     class="px-0 lg:py-68 md:py-32 sm:py-20 py-16 relative z-10 w-full bg-white opacity-0 hover:opacity-100 text-center rounded">
                     <h1 class="title-font text-lg font-medium text-gray-900 mb-3">10 Februari 2020 - Finishing Facade Kavling 7 </h1>
                  </a>
               </div>
            </div>
         </div>
      </div>
   </section>
<?php get_footer()?>