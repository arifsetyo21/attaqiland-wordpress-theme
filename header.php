<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package attaqitheme
 */
?>

<!DOCTYPE html>
<html lang="en">

<head>
   <meta charset="<?php bloginfo('charset');?>">
   <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

   <meta http-equiv="X-UA-Compatible" content="ie=edge">
   <meta name="google-site-verification" content="12s61jME1quMJMdF2GPuAMFAQhuADmqZe3UJRIiJ2aI" />
   <title>🏠 ATTAQI LUXURY SHARIA TOWNHOUSE</title>

   <link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.css">

   <!--<script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.min.js" defer></script> -->
   <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">

   <?php wp_head();?>

   <link rel="apple-touch-icon" sizes="180x180" href="<?php echo do_shortcode("[template_dir image='favicon_io/apple-touch-icon.png']"); ?>">
   <link rel="icon" type="image/png" sizes="32x32" href="<?php echo do_shortcode("[template_dir image='favicon_io/favicon-32x32.png']"); ?>">
   <link rel="icon" type="image/png" sizes="16x16" href="<?php echo do_shortcode("[template_dir image='favicon_io/favicon-16x16.png']"); ?>">
   <link rel="manifest" href="<?php echo do_shortcode("[template_dir image='favicon_io/site.webmanifest']"); ?>">
</head>
<!-- Swiper -->

<body>
   <button onclick="topFunction()" class="rounded bg-blue-550 hover:bg-yellow-500" id="scrollToFeatureButton"
      title="Go to top">Kembali ke Keunggulan ⬆️</button>
   <div class="flex flex-col" id="headerCarousel">
      <header class="bg-opacity-0 text-gray-400 bg-gray-900 w-full body-font overflow-hidden top-0 z-50 absolute" id="navbarFixed">
         <div class="container mx-auto flex flex-wrap flex-col md:flex-row items-center">
            <div class="w-full text-gray-700  dark-mode:text-gray-200 dark-mode:bg-gray-800">
               <div x-data="{ open: false }"
                  class="relative flex flex-col max-w-screen-xl px-4 mx-auto md:items-center md:justify-between md:flex-row md:px-6 lg:px-8">
                  <div class="relative p-0 flex flex-row items-center justify-between">
                     <a href="<?php echo get_bloginfo('url') ?>"
                        class="text-lg font-semibold tracking-widest text-gray-900 uppercase rounded-lg dark-mode:text-white focus:outline-none focus:shadow-outline"><img
                           alt="logo perumahan syariah attaqi" src="<?php echo do_shortcode("[template_dir image='LOGO+HD+(1).png']"); ?>" class="w-40 p-3" alt="" id="navbarLogo"></a>
                     <button class="md:hidden rounded-lg focus:outline-none focus:shadow-outline" @click="open = !open">
                        <svg fill="currentColor" viewBox="0 0 20 20" class="w-6 h-6">
                           <path x-show="!open" fill-rule="evenodd"
                              d="M3 5a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM9 15a1 1 0 011-1h6a1 1 0 110 2h-6a1 1 0 01-1-1z"
                              clip-rule="evenodd"></path>
                           <path x-show="open" fill-rule="evenodd"
                              d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                              clip-rule="evenodd"></path>
                        </svg>
                     </button>
                  </div>
                  <nav :class="{'flex': open, 'hidden': !open}"
                     class="relative flex-col flex-grow pb-4 md:pb-0 hidden md:flex md:justify-end md:flex-row text-white">
                     <a class="px-4 py-2 mt-2 text-sm font-normal bg-transparent rounded-lg md:mt-0 md:ml-4" href="#">About Us</a>
                     <a class="px-4 py-2 mt-2 text-sm font-normal bg-transparent rounded-lg md:mt-0 md:ml-4" href="/progress">Progres</a>
                     <div class="px-4 py-2 mt-2 text-sm font-normal bg-yellow-500 rounded-full md:mt-0 md:ml-4"><a
                           href="/pricelist">Booklet & Pricelist Request</a>
                           </div>
                  </nav>
               </div>
            </div>
         </div>
      </header>
