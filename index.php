<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package attaqitheme
 */

// get_header();
?>

	<!-- <main id="primary" class="site-main"> -->

		<?php
// if (have_posts()):

//  if (is_home() && !is_front_page()):
?>
							<!-- <header> -->
								<!-- <h1 class="page-title screen-reader-text"><?php single_post_title();?></h1> -->
							<!-- </header> -->
							<?php
// endif;

/* Start the Loop */
// while (have_posts()):
//  the_post();

/*
 * Include the Post-Type-specific template for the content.
 * If you want to override this in a child theme, then include a file
 * called content-___.php (where ___ is the Post Type name) and that will be used instead.
 */
//  get_template_part('template-parts/content', get_post_type());

// endwhile;

// the_posts_navigation();

// else:

//  get_template_part('template-parts/content', 'none');

// endif;
?>

	<!-- </main> #main -->
	<!doctype html>
<html>
<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
   <link href="https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css" rel="stylesheet">
	<link rel="stylesheet" href="https://attaqiland.com/wp-content/themes/attaqitheme/css/style.css">
   <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
   <meta property="og:title" content="Perumahan Muslim Syariah Solo">
   <meta property="og:site_name" content="ATTAQI LUXURY SHARIA TOWNHOUSE">
   <meta property="og:url" content="https://attaqiland.com">
   <meta property="og:description" content="Attaqi Land berkomitmen untuk menciptakan hunian berupa perumahan dan cluster Muslim, dengan pembayaran skema 100% Syariah tanpa riba dan menciptakan lingkungan yang berorientasi Dunia dan Akhirat.">
   <meta property="og:type" content="website">
   <meta property="og:image" content="https://attaqiland.com/wp-content/themes/attaqitheme/assets/img/Gallery/fasilitas-2-min.jpg">

   <style>

   </style>
</head>
<body >
   <header class="text-gray-400 bg-gray-900 body-font overflow-hidden">
      <div class="container mx-auto flex flex-wrap p-4 flex-col md:flex-row items-center">
         <a href="/" class="flex title-font font-medium items-center text-white mb-4 md:mb-0">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/LOGO+HD+(1).png" class="w-40 p-3" alt="">
         </a>
         <nav class="md:ml-auto flex flex-wrap items-center text-base justify-center">
            <a href="/" class="mr-5 hover:text-white">About Us</a>
            <a href="/progress" class="mr-5 hover:text-white">Progress</a>
         </nav>
         <a
            href="/pricelist" class="inline-flex items-center bg-yellow-600 border-0 py-1 px-3 focus:outline-none hover:bg-yellow-700 text-gray-900 md:mt-0 -mr-8 -pt-8 h-16 left-3 relative">Booklet & Pricelist Request
            <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
               class="w-4 h-4 ml-1" viewBox="0 0 24 24">
               <path d="M5 12h14M12 5l7 7-7 7"></path>
            </svg>
         </a>
      </div>
   </header>
   <section class="text-gray-400 bg-gray-900 body-font z-10 relative">
      <div class="container mx-auto flex px-10 pt-10 pb-24 flex-col items-center">
         <img data-aos="zoom-in" class="lg:w-full md:w-3/6 w-5/6 mb-10 object-cover object-center" alt="hero" src="<?php echo get_template_directory_uri(); ?>/assets/img/Cover-min-wecompress.jpg">
         <div data-aos="zoom-in" class="text-center lg:w-5/6 w-full">
            <h1 class="title-font sm:text-4xl text-3xl mb-4 font-medium text-white">Attaqi Land berkomitmen untuk menciptakan hunian berupa perumahan dan cluster Muslim, dengan pembayaran skema <span class="text-yellow-600"><strong>100%
            Syariah tanpa riba</strong></span> dan menciptakan lingkungan yang berorientasi Dunia dan Akhirat
               <br class="hidden lg:inline-block"></h1>
            <!-- <p class="leading-relaxed mb-8">Meggings kinfolk echo park stumptown DIY, kale chips beard jianbing tousled. Chambray dreamcatcher trust fund, kitsch vice godard disrupt ramps hexagon mustache umami snackwave tilde chillwave ugh. Pour-over meditation PBR&amp;B pickled ennui celiac mlkshk freegan photo booth af fingerstache pitchfork.</p> -->
            <div class="flex justify-center">
              <a  href="/#video-preview" class="inline-flex text-white border-0 py-2 px-6 focus:outline-none hover:bg-yellow-600 hover:text-bold hover:border-4 border rounded-full text-lg">Lihat Video Preview</a>
            </div>
         </div>
      </div>
   </section>
   <section class="text-gray-400 bg-gray-900 body-font overflow-hidden z-0 relative">
      <div class="container px-5 lg:pb-24 lg:pt-0 sm:pt-24 mx-auto">
         <div data-aos="fade-left" class="flex flex-wrap -m-4 lg:left-36 md:left-32 sm:left-14 relative">
            <div class="w-full p-4">
               <div class="bg-white bg-opacity-100 p-6 flex flex sm:flex-row flex-col">
                  <div class="lg:w-1/3 sm:w-2/3 sm:pr-6 lg:py-22 md:py-8 sm:mr-4 w-full text-center my-auto">
                     <h3 class="tracking-widest text-yellow-500 text-sm font-medium title-font">Setitik Mutiara</h3>
                     <h2 class="text-lg text-gray-700 font-bold title-font mb-4">QS. Al- Baqarah : 278</h2>
                     <p class="leading-relaxed text-gray-600 font-medium text-2xl">
                        Hai orang-orang yang beriman, bertakwalah kepada Allah dan tinggalkan sisa riba (yang belum dipungut) jika kamu
                        orang-orang yang beriman.</p>
                  </div>
                  <div class="md:w-2/3 sm:w-full -m-6">
                     <img loading="lazy" class="h-100 w-full object-cover object-center" src="<?php echo get_template_directory_uri(); ?>/assets/img/Fasilitas 3-min.jpg" alt="content">
                  </div>
               </div>
            </div>
         </div>
      </div>
   </section>
   <section class="text-gray-400 bg-gray-900 body-font">
      <div class="container px-5 pb-24 mx-auto flex flex-wrap">
         <div class="text-center mb-20">
            <h1 class="sm:text-3xl text-3xl font-bold title-font text-yellow-600 mb-4">Hunian Idaman Muslim</h1>
            <p class="text-base leading-relaxed xl:w-2/4 lg:w-3/4 mx-auto text-gray-500s text-lg">Keunggulan hunian Attaqi dibandingkan dengan hunian yang lain
               yang berupa perumahan dan cluster Muslim dengan lingkungan yang nyaman.</p>
            <div class="flex mt-6 justify-center">
               <div class="w-16 h-1 rounded-full bg-yellow-500 inline-flex"></div>
            </div>
         </div>
         <div data-aos="flip-left" class="lg:w-1/2 w-full mb-10 lg:mb-0 rounded-lg overflow-hidden">
            <img loading="lazy" alt="feature" class="object-cover object-center h-full w-full" src="<?php echo get_template_directory_uri(); ?>/assets/img/R.KELUARGA-jpg.jpg">
         </div>
         <div data-aos="fade-left" class="flex flex-col flex-wrap lg:py-6 -mb-10 lg:w-1/2 lg:pl-12 lg:text-left text-center">
            <div class="flex flex-col mb-10 lg:items-start items-center">
               <div
                  class="w-12 h-12 inline-flex items-center justify-center rounded-full bg-gray-800 text-yellow-500 mb-5">
                  <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                     class="w-6 h-6" viewBox="0 0 24 24">
                     <path d="M22 12h-4l-3 9L9 3l-3 9H2"></path>
                  </svg>
               </div>
               <div class="flex-grow">
                  <h2 class="text-white text-lg title-font font-semibold mb-3">Desain</h2>
                  <p class="leading-relaxed text-base">Desain yang compact dan modern, ruang disediakan dengan ukuran yang proper, mendapatkan pencahayaan dan sirkulasi udara yang melimpah dan detail instalasi yang sudah disediakan sehingga tidak perlu bongkar-bongkar saat instalasi home appliance seperti AC, Lampu dinding dll..</p>
               </div>
            </div>
            <div class="flex flex-col mb-10 lg:items-start items-center">
               <div
                  class="w-12 h-12 inline-flex items-center justify-center rounded-full bg-gray-800 text-yellow-500 mb-5">
                  <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                     class="w-6 h-6" viewBox="0 0 24 24">
                     <circle cx="6" cy="6" r="3"></circle>
                     <circle cx="6" cy="18" r="3"></circle>
                     <path d="M20 4L8.12 15.88M14.47 14.48L20 20M8.12 8.12L12 12"></path>
                  </svg>
               </div>
               <div class="flex-grow">
                  <h2 class="text-white text-lg title-font font-semibold mb-3">Kualitas Material</h2>
                  <p class="leading-relaxed text-base">Kami menggunakan material terbaik dan berkelas, karena penggunaan material tidak hanya mempengaruhi penampilan dan presitige, tetapi juga durabilitas dan masa pemakaian, serta instalasi yang dilakukan oleh ahli sehingga menghasilkan rumah kesayangan yang kokoh dan keren, cek dan buktikan langsung !.</p>
               </div>
            </div>
            <div class="flex flex-col mb-10 lg:items-start items-center">
               <div
                  class="w-12 h-12 inline-flex items-center justify-center rounded-full bg-gray-800 text-yellow-500 mb-5">
                  <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                     class="w-6 h-6" viewBox="0 0 24 24">
                     <path d="M20 21v-2a4 4 0 00-4-4H8a4 4 0 00-4 4v2"></path>
                     <circle cx="12" cy="7" r="4"></circle>
                  </svg>
               </div>
               <div class="flex-grow">
                  <h2 class="text-white text-lg title-font font-semibold mb-3">Pelayanan dan Fasilitas</h2>
                  <p class="leading-relaxed text-base">Kami tidak hanya jualan rumah, namun juga pelayanan tiada henti, contohnya fasilitas secara fisik berupa Mushola/Masjid in site dan juga aktivitas kami sediakan di dalamnya seperti pengajian rutin dan rumah Tahfidz, termasuk pelayanan keluh kesah seputar bangunan akan kami tangani sebaik-baiknya, InsyaAllah kami selalu menjalin hubungan baik dan silaturahim dengan seluruh pembeli dan pemilik.</p>
               </div>
            </div>
         </div>
      </div>
   </section>
   <section class="text-gray-600 body-font" id="video-preview">
      <div class="container px-5 py-16 mx-auto flex flex-col">
         <div data-aos="zoom-in" class="lg:w-4/6 mx-auto">
            <div class="text-center mb-10">
               <h1 class="sm:text-3xl text-3xl font-bold title-font text-yellow-600 ">Hunian Idaman Muslim</h1>
               <!-- <p class="text-base leading-relaxed xl:w-2/4 lg:w-3/4 mx-auto text-gray-500s text-lg">Keunggulan hunian Attaqi
                  dibandingkan dengan hunian yang lain
                  yang berupa perumahan dan cluster Muslim dengan lingkungan yang nyaman.</p> -->
            </div>
            <div class="rounded-lg h-96 overflow-hidden">
               <iframe class="object-cover object-center h-full w-full" src="https://www.youtube.com/embed/GUP4ougiefQ?start=8" title="YouTube video player"
                  frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                  allowfullscreen></iframe>
               <!-- <img alt="content" class="object-cover object-center h-full w-full" src="https://dummyimage.com/1200x500"> -->
            </div>
            <div class="flex mt-6 justify-center">
               <div class="w-16 h-1 rounded-full bg-yellow-500 inline-flex"></div>
            </div>
         </div>
      </div>
   </section>
   <section class="text-gray-400 bg-gray-900 body-font">
      <div class="container px-5 py-24 mx-auto">
         <div class="flex flex-col text-center w-full mb-20">
            <h1 class="sm:text-3xl text-2xl font-medium title-font mb-4 text-white">Tipe Luxury</h1>
            <p class="lg:w-2/3 mx-auto leading-relaxed text-base">Menggunakan material dengan brand terbaik, fitur dan desain mengedepankan sentuhan mewah diberikan untuk meningkatkan eksklusifitas di setiap sudut dan aktivitas di dalam maupun luar rumah.</p>
         </div>
         <div class="flex flex-wrap -m-4">
            <div data-aos="flip-down" class="lg:w-1/2 sm:w-1/2 p-4">
               <div class="flex relative">
                  <img loading="lazy" alt="gallery" class="absolute inset-0 w-full h-full object-cover object-center"
                     src="<?php echo get_template_directory_uri(); ?>/assets/img/Luxury 1.png">
                  <div
                     class="px-8 py-10 relative z-10 w-full border-4 border-gray-800 bg-gray-900 opacity-0 hover:opacity-70">
                     <h2 class="tracking-widest text-sm title-font font-medium text-yellow-400 mb-1">FITUR</h2>
                     <h1 class="title-font text-lg font-medium text-white mb-3">Tata Letak yang Serba Terjangkau</h1>
                     <p class="leading-relaxed">Tata letak ruangan yang serba terjangkau dan memberi kemudahan workflow serta jangkauan antara ruang keluarga, daput, kamar mandi, inner court & akses tangga.</p>
                  </div>
               </div>
            </div>
            <div data-aos="flip-up" class="lg:w-1/2 sm:w-1/2 p-4">
               <div class="flex relative">
                  <img loading="lazy" alt="gallery" class="absolute inset-0 w-full h-full object-cover object-center"
                     src="<?php echo get_template_directory_uri(); ?>/assets/img/Luxury 2.png">
                  <div
                     class="px-8 py-10 relative z-10 w-full border-4 border-gray-800 bg-gray-900 opacity-0 hover:opacity-70">
                     <h2 class="tracking-widest text-sm title-font font-medium text-yellow-400 mb-1">FITUR</h2>
                     <h1 class="title-font text-lg font-medium text-white mb-3">Bagian Atap yang dapat difungsikan</h1>
                     <p class="leading-relaxed">kegagahan wajah bagian atap pada tipe ini bukan hanya tampilan lagi, namun difungsikan sebagai kamar dan kantor kecil yang terletak di area loteng, memberi ruangan yang lebih privat dan lebih keren.</p>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </section>
   <section class="text-gray-600 body-font">
      <div class="container px-5 py-24 mx-auto">
         <div class="flex flex-col text-center w-full mb-20">
            <h1 class="sm:text-3xl text-2xl font-medium title-font mb-4 text-gray-900">Tipe Deluxe</h1>
            <p class="lg:w-2/3 mx-auto leading-relaxed text-base">Tipe menengah yang mengutamakan kenyamanan dalam keluarga, akses ruangan yang rapi dan elegan.</p>
         </div>
         <div class="flex flex-wrap -m-4">
            <div data-aos="flip-down" class="lg:w-1/3 sm:w-1/2 p-4">
               <div class="flex relative">
                  <img loading="lazy" alt="gallery" class="absolute inset-0 w-full h-full object-cover object-center"
                     src="<?php echo get_template_directory_uri(); ?>/assets/img/Deluxe 1-min.jpg">
                  <div
                     class="px-8 py-10 relative z-10 w-full border-4 border-gray-200 bg-white opacity-0 hover:opacity-100">
                     <h2 class="tracking-widest text-sm title-font font-medium text-yellow-500 mb-1">FITUR</h2>
                     <h1 class="title-font text-lg font-medium text-gray-900 mb-3">Pencahayaan dan Sirkulasi Udara yang Maksimal</h1>
                     <p class="leading-relaxed">Pencahayaan dan sirkulasi udara maksimal dapat menjangkau seluruh area ruang keluarga dan dapur.</p>
                  </div>
               </div>
            </div>
            <div data-aos="flip-up" class="lg:w-1/3 sm:w-1/2 p-4">
               <div class="flex relative">
                  <img loading="lazy" alt="gallery" class="absolute inset-0 w-full h-full object-cover object-center"
                     src="<?php echo get_template_directory_uri(); ?>/assets/img/Deluxe 2-min.jpg">
                  <div
                     class="px-8 py-10 relative z-10 w-full border-4 border-gray-200 bg-white opacity-0 hover:opacity-100">
                     <h2 class="tracking-widest text-sm title-font font-medium text-yellow-500 mb-1">FITUR</h2>
                     <h1 class="title-font text-lg font-medium text-gray-900 mb-3">Design Kamar yang Elegant</h1>
                     <p class="leading-relaxed">Kamar yang compact dan elegan dengan jendela yang unik.</p>
                  </div>
               </div>
            </div>
            <div data-aos="flip-down" class="lg:w-1/3 sm:w-1/2 p-4">
               <div class="flex relative">
                  <img loading="lazy" alt="gallery" class="absolute inset-0 w-full h-full object-cover object-center"
                     src="<?php echo get_template_directory_uri(); ?>/assets/img/Deluxe 3.jpg">
                  <div
                     class="px-8 py-10 relative z-10 w-full border-4 border-gray-200 bg-white opacity-0 hover:opacity-100">
                     <h2 class="tracking-widest text-sm title-font font-medium text-yellow-500 mb-1">FITUR</h2>
                     <h1 class="title-font text-lg font-medium text-gray-900 mb-3">Optimasi Setiap Sudut Ruangan</h1>
                     <p class="leading-relaxed">Optimasi setiap sudut ruangan agar dapat difungsikan dengan baik.</p>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </section>
   <section class="text-gray-400 bg-gray-900 body-font">
      <div class="container px-5 py-24 mx-auto">
         <div class="flex flex-col text-center w-full mb-20">
            <h1 class="sm:text-3xl text-2xl font-medium title-font mb-4 text-white">Tipe Basic</h1>
            <p class="lg:w-2/3 mx-auto leading-relaxed text-base">Tipe yang paling pas untuk memulai keluarga baru, desain compact dan elegan, fitur tetap sama dengan tipe tertinggi, namun dengan sentuhan personal dan minimalis.</p>
         </div>
         <div class="flex flex-wrap -m-4">
            <div data-aos="flip-down" class="lg:w-1/3 sm:w-1/2 p-4">
               <div class="flex relative">
                  <img loading="lazy" alt="gallery" class="absolute inset-0 w-full h-full object-cover object-center"
                     src="<?php echo get_template_directory_uri(); ?>/assets/img/Basic 1-min.jpg">
                  <div
                     class="px-8 py-10 relative z-10 w-full border-4 border-gray-800 bg-gray-900 opacity-0 hover:opacity-70">
                     <h2 class="tracking-widest text-sm title-font font-medium text-yellow-400 mb-1">FITUR</h2>
                     <h1 class="title-font text-lg font-medium text-white mb-3">Design Minimalis</h1>
                     <p class="leading-relaxed">ruangan dengan desain minimalis, mempermudah akses dan menambah kenyamanan.</p>
                  </div>
               </div>
            </div>
            <div data-aos="flip-up" class="lg:w-1/3 sm:w-1/2 p-4">
               <div class="flex relative">
                  <img loading="lazy" alt="gallery" class="absolute inset-0 w-full h-full object-cover object-center"
                     src="<?php echo get_template_directory_uri(); ?>/assets/img/Basic 2-min.jpg">
                  <div
                     class="px-8 py-10 relative z-10 w-full border-4 border-gray-800 bg-gray-900 opacity-0 hover:opacity-70">
                     <h2 class="tracking-widest text-sm title-font font-medium text-yellow-400 mb-1">FITUR</h2>
                     <h1 class="title-font text-lg font-medium text-white mb-3">Menghadap Matahari Terbit</h1>
                     <p class="leading-relaxed">Kamar tidur utama diberikan akses jendela menghadap matahari terbit.</p>
                  </div>
               </div>
            </div>
            <div data-aos="flip-down" class="lg:w-1/3 sm:w-1/2 p-4">
               <div class="flex relative">
                  <img loading="lazy" alt="gallery" class="absolute inset-0 w-full h-full object-cover object-center"
                     src="<?php echo get_template_directory_uri(); ?>/assets/img/Basic 3-min.jpg">
                  <div
                     class="px-8 py-10 relative z-10 w-full border-4 border-gray-800 bg-gray-900 opacity-0 hover:opacity-70">
                     <h2 class="tracking-widest text-sm title-font font-medium text-yellow-400 mb-1">FITUR</h2>
                     <h1 class="title-font text-lg font-medium text-white mb-3">Outdoor Kitchen Menyatu dengan Inner Court</h1>
                     <p class="leading-relaxed">Outdoor kitchen dan inner court menyatu, memberikan sentuhan rasa yang leluasa dan siap untuk di kreasikan.</p>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </section>
   <section class="text-gray-600 body-font">
      <div class="container px-5 py-24 mx-auto">
         <div class="text-center mb-20">
            <h1 class="sm:text-3xl text-2xl font-bold title-font text-gray-900 mb-4">Perumahan dengan Skema 100% Syariah</h1>
            <!-- <p class="text-base leading-relaxed xl:w-2/4 lg:w-3/4 mx-auto text-gray-500s">Blue bottle crucifix vinyl -->
               <!-- post-ironic four dollar toast vegan taxidermy. Gastropub indxgo juice poutine, ramps microdosing banh mi -->
               <!-- pug.</p> -->
            <div class="flex mt-6 justify-center">
               <div class="w-16 h-1 rounded-full bg-yellow-500 inline-flex"></div>
            </div>
            <div class="rounded-lg w-7/12 mt-4 overflow-hidden mx-auto">
               <img loading="lazy" alt="content" class="object-cover object-center h-full w-full" src="<?php echo get_template_directory_uri(); ?>/assets/img/denah-2.jpg">
            </div>
         </div>
         <div class="flex flex-wrap sm:-m-4 -mx-4 -mb-10 -mt-4 md:space-y-0 space-y-6">
            <div data-aos="zoom-in" class="p-4 md:w-1/3 flex flex-col text-center items-center">
               <div
                  class="w-20 h-20 inline-flex items-center justify-center rounded-full bg-yellow-100 text-yellow-500 mb-5 flex-shrink-0">
                  <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                     class="w-10 h-10" viewBox="0 0 24 24">
                     <path d="M22 12h-4l-3 9L9 3l-3 9H2"></path>
                  </svg>
               </div>
               <div class="flex-grow">
                  <h2 class="text-gray-900 text-lg title-font font-medium mb-3">Tanpa Bank & BI Checking</h2>
                  <p class="leading-relaxed text-base">memberikan kemudahan kepada calon pembeli yang kesulitan melalui sistem BI Checking.</p>
                  <a class="mt-3 text-yellow-500 inline-flex items-center">Learn More
                     <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                        class="w-4 h-4 ml-2" viewBox="0 0 24 24">
                        <path d="M5 12h14M12 5l7 7-7 7"></path>
                     </svg>
                  </a>
               </div>
            </div>
            <div data-aos="zoom-in" class="p-4 md:w-1/3 flex flex-col text-center items-center">
               <div
                  class="w-20 h-20 inline-flex items-center justify-center rounded-full bg-yellow-100 text-yellow-500 mb-5 flex-shrink-0">
                  <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                     class="w-10 h-10" viewBox="0 0 24 24">
                     <circle cx="6" cy="6" r="3"></circle>
                     <circle cx="6" cy="18" r="3"></circle>
                     <path d="M20 4L8.12 15.88M14.47 14.48L20 20M8.12 8.12L12 12"></path>
                  </svg>
               </div>
               <div class="flex-grow">
                  <h2 class="text-gray-900 text-lg title-font font-medium mb-3">Tanpa bunga</h2>
                  <p class="leading-relaxed text-base">Dalam pembelian properti syariah tidak ada suku bunga tiap tahunnya, meskipun cicilan sampai bertahun
                  tahun, cicilan pembayarannya akan flat sampai lunas, sehingga menghindari praktek riba.</p>
                  <a class="mt-3 text-yellow-500 inline-flex items-center">Learn More
                     <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                        class="w-4 h-4 ml-2" viewBox="0 0 24 24">
                        <path d="M5 12h14M12 5l7 7-7 7"></path>
                     </svg>
                  </a>
               </div>
            </div>
            <div data-aos="zoom-in" class="p-4 md:w-1/3 flex flex-col text-center items-center">
               <div
                  class="w-20 h-20 inline-flex items-center justify-center rounded-full bg-yellow-100 text-yellow-500 mb-5 flex-shrink-0">
                  <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                     class="w-10 h-10" viewBox="0 0 24 24">
                     <path d="M20 21v-2a4 4 0 00-4-4H8a4 4 0 00-4 4v2"></path>
                     <circle cx="12" cy="7" r="4"></circle>
                  </svg>
               </div>
               <div class="flex-grow">
                  <h2 class="text-gray-900 text-lg title-font font-medium mb-3">Tanpa denda dan penalti</h2>
                  <p class="leading-relaxed text-base">Dalam properti syariah tidak boleh ada denda jika terjadi keterlambatan pembayaran, dalam jual
                  beli sejatinya adalah hutang piutang, jika mengambil keuntungan dari hutang yaitu riba.</p>
                  <a class="mt-3 text-yellow-500 inline-flex items-center">Learn More
                     <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                        class="w-4 h-4 ml-2" viewBox="0 0 24 24">
                        <path d="M5 12h14M12 5l7 7-7 7"></path>
                     </svg>
                  </a>
               </div>
            </div>
            <div data-aos="zoom-in" class="p-4 md:w-1/2 flex flex-col text-center items-center">
               <div
                  class="w-20 h-20 inline-flex items-center justify-center rounded-full bg-yellow-100 text-yellow-500 mb-5 flex-shrink-0">
                  <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                     class="w-10 h-10" viewBox="0 0 24 24">
                     <path d="M20 21v-2a4 4 0 00-4-4H8a4 4 0 00-4 4v2"></path>
                     <circle cx="12" cy="7" r="4"></circle>
                  </svg>
               </div>
               <div class="flex-grow">
                  <h2 class="text-gray-900 text-lg title-font font-medium mb-3">Tanpa sita</h2>
                  <p class="leading-relaxed text-base">Dalam Properti Syariah tidak boleh melakukan sita unit jika pembeli tidak sanggup membayar cicilan lagi. Karena unit tersebut sudah sepenuhnya milik pembeli walaupun masih kredit. Maka akan dilakukan diskusi dan akan ada solusi bersama Developer</p>
                  <a class="mt-3 text-yellow-500 inline-flex items-center">Learn More
                     <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                        class="w-4 h-4 ml-2" viewBox="0 0 24 24">
                        <path d="M5 12h14M12 5l7 7-7 7"></path>
                     </svg>
                  </a>
               </div>
            </div>
            <div data-aos="zoom-in" class="p-4 md:w-1/2 flex flex-col text-center items-center">
               <div
                  class="w-20 h-20 inline-flex items-center justify-center rounded-full bg-yellow-100 text-yellow-500 mb-5 flex-shrink-0">
                  <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                     class="w-10 h-10" viewBox="0 0 24 24">
                     <path d="M20 21v-2a4 4 0 00-4-4H8a4 4 0 00-4 4v2"></path>
                     <circle cx="12" cy="7" r="4"></circle>
                  </svg>
               </div>
               <div class="flex-grow">
                  <h2 class="text-gray-900 text-lg title-font font-medium mb-3">Tanpa Akad Bathil</h2>
                  <p class="leading-relaxed text-base">Seluruh akad dan pasal-pasal dalam perjanjian tidak ada yang menggantung dan samar, semuanya jelas dan terukur.</p>
                  <a class="mt-3 text-yellow-500 inline-flex items-center">Learn More
                     <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                        class="w-4 h-4 ml-2" viewBox="0 0 24 24">
                        <path d="M5 12h14M12 5l7 7-7 7"></path>
                     </svg>
                  </a>
               </div>
            </div>
         </div>
      </div>
   </section>
   <section class="text-gray-300 bg-gray-900 body-font">
      <div class="container mx-auto flex px-5 py-24 items-center justify-center flex-col">
         <!-- <img class="lg:w-2/6 md:w-3/6 w-5/6 mb-10 object-cover object-center rounded" alt="hero" -->
            <!-- src="<?php echo get_template_directory_uri(); ?>/assets/img/CapitalGain.jpg"> -->
            <div class="text-center lg:w-2/3 w-full">
               <h1 class="title-font sm:text-4xl text-3xl mb-4 font-semibold text-gray-900 bg-yellow-500 rounded p-4 inline-block">Harga Properti Terus Naik!</h1>
               <p class="leading-relaxed mb-8 text-justify">
                  Di Daerah Gentan, kenaikan harga properti per tahunnya mencapai 30%, saat ini tahap pembangunan Attaqi Gentan sedang
               pada tahap awal (pembangunan rumah contoh) dan diperkirakan selesai pada 30 Januari 2021, sehingga dipastikan harga akan
               naik setelah itu, karena mulai memasuki tahap ke 2 (pembangunan rumah selanjutnya) dan saat semua rumah sudah jadi
               (ready unit) maka harga akan melambung mengikuti harga pasar rumah rumah disekitar.</p>

               <p class="leading-relaxed mb-8 text-justify">
                  Saat terbaik membeli rumah adalah saat masih tahap awal (tahap persiapan lahan & pembangunan rumah contoh) karena harga
                  masih jauh lebih rendah dibandingkan unit jadi, dan akan ada nilai investasi yang luar biasa, bahkan saat anda belum
                  menempati rumah yang baru saja kami serah terimakan dan langsung dijual lagi, anda bisa meraup untung sekitar 60-80%
                  (belum termasuk kenaikan harga pasar & inflasi), maka properti adalah aset terbaik dan aman.
               </p>

               <p class="leading-relaxed mb-8 text-justify">
                  Masih berfikir kalau investasi properti itu merugikan ?
                  Semoga Tidak ! Karena properti lah yang akan MENYELAMATKAN KEUANGAN Anda di masa depan. Insya Allah
                  Investasi atau bisnis bukan hanya sekedar UNTUNG dan RUGI saja, tetapi ada yang lebih penting dari itu semua, yaitu
                  mengenai SURGA dan NERAKA. Disini Kami sangat mengedepankan proses PEMBELIAN SECARA SYARIAH sehingga TERHINDAR dari DOSA
                  RIBA dan AKAD BATHIL.</p>
               </p>
               <div class="flex text-left flex-col sm:flex-row sm:items-center items-start mx-auto">
                  <h1 class="flex-grow sm:pr-16 text-2xl font-semibold title-font text-white"><span class="underline">Segera hubungi tim marketing kami</span> untuk mengetahui promo lainnya, dan jadwal gathering atau survei online.</h1>
                  <button
                     class="flex-shrink-0 text-gray-900 bg-yellow-500 border-0 py-2 px-8 focus:outline-none hover:bg-yellow-600 rounded-full text-lg mt-10 sm:mt-0 font-bold"><a href="/pricelist">Booklet & Pricelist
                     <br />Request</a></button>
               </div>
         </div>
      </div>
   </section>
   <section class="text-gray-600 body-font relative">
      <div class="absolute inset-0 bg-gray-300">
         <iframe width="90%" height="100%" frameborder="0" marginheight="0" marginwidth="0" title="map" scrolling="no" loading="lazy"
            src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d7909.9281070789475!2d110.7763991!3d-7.5788919!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xec975dfd35c3b0bd!2sATTAQI%20SHARIA%20TOWNHOUSE!5e0!3m2!1sen!2sid!4v1617894992295!5m2!1sen!2sid"
            style="filter: grayscale(1) contrast(1.2) opacity(0.4);"></iframe>
      </div>
      <div class="container px-5 py-24 mx-auto flex">
         <div
            class="lg:w-1/3 md:w-1/2 bg-white rounded-lg p-8 flex flex-col md:ml-auto w-full mt-10 md:mt-0 relative z-10 shadow-md">
            <h2 class="text-gray-900 text-lg mb-1 font-medium title-font">Feedback</h2>
            <p class="leading-relaxed mb-5 text-gray-600">Post-ironic portland shabby chic echo park, banjo fashion axe</p>
            <div class="relative mb-4">
               <label for="email" class="leading-7 text-sm text-gray-600">Email</label>
               <input type="email" id="email" name="email"
                  class="w-full bg-white rounded border border-gray-300 focus:border-indigo-500 focus:ring-2 focus:ring-indigo-200 text-base outline-none text-gray-700 py-1 px-3 leading-8 transition-colors duration-200 ease-in-out">
            </div>
            <div class="relative mb-4">
               <label for="message" class="leading-7 text-sm text-gray-600">Message</label>
               <textarea id="message" name="message"
                  class="w-full bg-white rounded border border-gray-300 focus:border-indigo-500 focus:ring-2 focus:ring-indigo-200 h-32 text-base outline-none text-gray-700 py-1 px-3 resize-none leading-6 transition-colors duration-200 ease-in-out"></textarea>
            </div>
            <a class="text-gray-700 text-center bg-yellow-500 border-0 py-2 px-6 focus:outline-none hover:bg-yellow-600 hover:text-white rounded-full text-lg" href="mailto:halo@bedasama.com?">
               <button class="font-semibold">Send Email</button>
            </a>
            <p class="text-xs text-gray-500 mt-3 text-center">Or</p>
            <a class="text-gray-700 inline text-center bg-yellow-500 border-0 mt-3 py-2 px-6 focus:outline-none hover:bg-yellow-600 hover:text-white rounded-full text-lg"
            href="https://mywa.link/attaqiland" target="_blank">
            <button class="font-semibold">Send Whatsapp </button>
            </a>
         </div>
      </div>
   </section>
   <footer class="text-gray-400 bg-gray-900 body-font overflow-hidden">
      <div class="bg-white md:ml-40 mt-20 px-10 ">
         <div class="flex flex-row">
            <div
               class="sm:pb-24 md:pb-12 pt-12 w-full flex md:items-center lg:items-start md:flex-row md:flex-nowrap flex-wrap flex-col">
               <div class="w-64 flex-shrink-0 md:mx-0 mx-auto text-center md:text-left">
                  <a href="/" class="flex title-font font-medium items-center md:justify-start justify-center text-white">
                     <img src="<?php echo get_template_directory_uri(); ?>/assets/img/LOGO+HD+(1).png" class="w-50 h-auto" alt="" srcset="">
                  </a>
                  <!-- <p class="mt-2 text-sm text-gray-500">Hunian Idaman Muslim</p> -->
               </div>
               <div
                  class="flex-grow flex w-full flex-wrap md:pl-20 -mb-10 md:mt-0 mt-10 md:text-left text-center justify-between">
                  <div class="lg:w-1/2 md:w-1/2  flex-grow w-full px-4">
                     <h2 class="title-font font-medium text-gray-800 tracking-widest text-lg mb-3">Kontak</h2>
                     <nav class="list-none mb-10">
                        <li>
                           <a href="mailto:attaqiland@gmail.com" class="text-gray-400 hover:text-yellow-500 hover:underline">attaqiland@gmail.com</a>
                        </li>
                        <li>
                           <a href="tel:081310202994" class="text-gray-400 hover:text-yellow-500 hover:underline">+62 813 1020 2994</a>
                        </li>
                        <li>
                           <a class="text-gray-400 hover:text-yellow-500 hover:underline">Setiap Hari (08.00 - 16.00
                              wib)</a>
                        </li>
                        <li>
                           <a href="https://goo.gl/maps/RKonCw6xYnKcr1TB6" class="text-gray-400 hover:text-yellow-500 hover:underline">Jl. Nakula, Gentan-Purbayan,
                              Baki, Sukoharjo</a>
                        </li>
                     </nav>
                  </div>
                  <div class="lg:w-1/2 md:w-1/2 w-full px-4">
                     <!-- <h2 class="title-font font-medium text-white tracking-widest text-sm mb-3">CATEGORIES</h2> -->
                     <nav class="list-none mb-10">
                        <li>
                           <a href="/" class="text-gray-400 hover:text-yellow-500 hover:underline">About Us</a>
                        </li>
                        <li>
                           <a href="/progress" class="text-gray-400 hover:text-yellow-500 hover:underline">Progress</a>
                        </li>
                        <li>
                           <a href="/pricelist" class="text-gray-400 hover:text-yellow-500 hover:underline">Booklet & Pricelist Request</a>
                        </li>
                     </nav>
                  </div>
               </div>
            </div>
         </div>
         <div class="flex flex-row pb-10">
            <div class="w-full flex md:items-center lg:items-start md:flex-row md:flex-nowrap flex-wrap flex-col">
               <div class="w-64 flex-shrink-0 md:mx-0 mx-auto text-center md:text-left">
                  <span class="inline-flex sm:ml-auto sm:mt-0 mt-2 justify-center sm:justify-start">
                     <a target="_blank" class="text-gray-400 mr-3  w-10 h-10 rounded-full text-center p-2" href="https://www.facebook.com/Attaqi-Sharia-Townhouse-107068631007113/">
                        <svg fill="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                           class="w-5 h-5" viewBox="0 0 24 24">
                           <path d="M18 2h-3a5 5 0 00-5 5v3H7v4h3v8h4v-8h3l1-4h-4V7a1 1 0 011-1h3z"></path>
                        </svg>
                     </a>
                     <a target="_blank" class="text-gray-400 mr-3  w-10 h-10 rounded-full text-center p-2" href="https://www.instagram.com/attaqi_land/">
                        <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round"
                           stroke-width="2" class="w-5 h-5" viewBox="0 0 24 24">
                           <rect width="20" height="20" x="2" y="2" rx="5" ry="5"></rect>
                           <path d="M16 11.37A4 4 0 1112.63 8 4 4 0 0116 11.37zm1.5-4.87h.01"></path>
                        </svg>
                     </a>
                  </span>
               </div>
               <div
                  class="flex-grow flex flex-wrap md:pl-20 -mb-10 md:mt-0 mt-10 md:text-left text-center justify-between">
                  <div class="lg:w-1/2 md:w-1/2 w-full px-4">
                     <p class="text-gray-400 text-sm text-center sm:text-left font-bold">Hunian Idaman Muslim</p>
                  </div>
                  <div class="lg:w-1/2 md:w-1/2 w-full px-4">
                     <p class="text-gray-400 text-sm text-center sm:text-left">© 2021 attaqiland.com —
                        <a target="_blank" href="https://www.instagram.com/attaqi_land/" rel="noopener noreferrer"
                           class="text-gray-500 ml-1" target="_blank">Attaqi Luxury Sharia Townhouse</a>
                     </p>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </footer>
   <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
   <script>
      AOS.init();
   </script>
</body>
</html>

<?php
// get_sidebar();
// get_footer();
