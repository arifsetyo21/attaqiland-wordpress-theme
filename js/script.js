//Get the button:
const scrollToFeatureButton = document.getElementById( 'scrollToFeatureButton' );
const headerCarousel = document.getElementById( 'headerCarousel' );
const navbarFixed = document.getElementById( 'navbarFixed' );
const feature = document.getElementById( 'feature' );
const navbarLogo = document.getElementById( 'navbarLogo' );
const navbarPricelistButton = document.querySelector( '#navbarFixed > div > div > div > nav > div' );
const featurePositionOnScrollTop = feature == null ? screen.height : feature.offsetTop - 30;
const headerCarouselValuePosition = ( headerCarousel == null ) ? screen.height : headerCarousel.offsetHeight;

// When the user scrolls down 20px from the top of the document, show the button
window.onscroll = function() {
	scrollFunction();
};

function scrollFunction() {
	if ( document.body.scrollTop > featurePositionOnScrollTop || document.documentElement.scrollTop > featurePositionOnScrollTop ) {
		scrollToFeatureButton.style.display = 'block';
	} else {
		scrollToFeatureButton.style.display = 'block';
	}

	if ( document.body.scrollTop > 0 || document.documentElement.scrollTop > 0 ) {
		navbarFixed.classList.remove( 'absolute' );
		navbarFixed.classList.add( 'fixed' );
	} else {
		navbarFixed.classList.add( 'absolute' );
		navbarFixed.classList.remove( 'fixed' );
	}

	if ( document.body.scrollTop > headerCarouselValuePosition || document.documentElement.scrollTop > headerCarouselValuePosition ) {
		navbarFixed.classList.add( 'bg-blue-550' );
		navbarLogo.classList.add( 'w-24' );
		navbarLogo.classList.remove( 'w-40' );
	} else {
		navbarFixed.classList.remove( 'bg-blue-550' );
		navbarLogo.classList.remove( 'w-24' );
		navbarLogo.classList.add( 'w-40' );
	}
}

// When the user clicks on the button, scroll to the top of the document
function topFunction() {
	document.body.scrollTop = featurePositionOnScrollTop; // For Safari
	document.documentElement.scrollTop = featurePositionOnScrollTop; // For Chrome, Firefox, IE and Opera
}

const galleryTop1 = new Swiper( '.gallery-top1', {
	spaceBetween: 10,
	navigation: {
		nextEl: '.swiper-button-next1',
		prevEl: '.swiper-button-prev1',
	},
	loop: true,
	loopedSlides: 4,
	autoplay: {
		delay: 10000,
		disableOnInteraction: false,
	},
} );

const galleryThumbs1 = new Swiper( '.gallery-thumbs1', {
	spaceBetween: 10,
	centeredSlides: true,
	slidesPerView: 'auto',
	touchRatio: 0.2,
	slideToClickedSlide: true,
	loop: true,
	loopedSlides: 4,
} );

galleryTop1.controller.control = galleryThumbs1;
galleryThumbs1.controller.control = galleryTop1;

const galleryTop2 = new Swiper( '.gallery-top2', {
	spaceBetween: 10,
	navigation: {
		nextEl: '.swiper-button-next',
		prevEl: '.swiper-button-prev',
	},
	loop: true,
	loopedSlides: 4,
	autoplay: {
		delay: 10000,
		disableOnInteraction: false,
	},
} );

const galleryThumbs2 = new Swiper( '.gallery-thumbs2', {
	spaceBetween: 10,
	centeredSlides: true,
	slidesPerView: 'auto',
	touchRatio: 0.2,
	slideToClickedSlide: true,
	loop: true,
	loopedSlides: 4,
} );

galleryTop2.controller.control = galleryThumbs2;
galleryThumbs2.controller.control = galleryTop2;

/*
  * Light YouTube Embeds by @labnol
  * Credit: https://www.labnol.org/
  */

function labnolIframe( div ) {
	const iframe = document.createElement( 'iframe' );
	iframe.setAttribute(
		'src',
		'https://www.youtube.com/embed/' + div.dataset.id + '?autoplay=1&rel=0'
	);
	iframe.setAttribute( 'frameborder', '0' );
	iframe.setAttribute( 'allowfullscreen', '1' );
	iframe.setAttribute(
		'allow',
		'accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture'
	);
	iframe.setAttribute( 'width', '1152' );
	iframe.setAttribute( 'height', '600' );
	// width="1152" height="600" frameborder="0"
	div.parentNode.replaceChild( iframe, div );
}

function initYouTubeVideos() {
	const playerElements = document.getElementsByClassName( 'youtube-player' );
	for ( let n = 0; n < playerElements.length; n++ ) {
		const videoId = playerElements[ n ].dataset.id;
		const div = document.createElement( 'div' );
		div.setAttribute( 'data-id', videoId );
		const thumbNode = document.createElement( 'img' );

		// thumbNode.src = 'https://attaqiland.com/src/img/Gallery/fasilitas-1-min.jpg';
		thumbNode.src = '//i.ytimg.com/vi/ID/sddefault.jpg'.replace(
			'ID',
			videoId
		);
		div.appendChild( thumbNode );
		const playButton = document.createElement( 'div' );
		playButton.setAttribute( 'class', 'play' );
		div.appendChild( playButton );
		div.onclick = function() {
			labnolIframe( this );
		};
		playerElements[ n ].appendChild( div );
	}
}

document.addEventListener( 'DOMContentLoaded', initYouTubeVideos );
