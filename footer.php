<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package attaqitheme
 */

?>

<footer class="text-gray-400 bg-blue-550 body-font overflow-hidden">
      <div class="bg-white md:ml-40 mt-20 px-10 ">
         <div class="flex flex-row">
            <div
               class="sm:pb-24 md:pb-12 pt-12 w-full flex md:items-center lg:items-start md:flex-row md:flex-nowrap flex-wrap flex-col">
               <div class="w-64 flex-shrink-0 md:mx-0 mx-auto text-center md:text-left">
                  <a href="/"
                     class="flex title-font font-medium items-center md:justify-start justify-center text-white">
                     <img src="<?php echo do_shortcode("[template_dir image='LOGO+HD+(1).png']"); ?>" class="w-50 h-auto" alt="" srcset="">
                  </a>
                  <!-- <p class="mt-2 text-sm text-gray-500">Hunian Idaman Muslim</p> -->
               </div>
               <div
                  class="flex-grow flex w-full flex-wrap md:pl-20 -mb-10 md:mt-0 mt-10 md:text-left text-center justify-between">
                  <div class="lg:w-1/2 md:w-1/2  flex-grow w-full px-4">
                     <h2 class="title-font font-medium text-gray-800 tracking-widest text-lg mb-3">Kontak</h2>
                     <nav class="list-none mb-10">
                        <li>
                           <a href="mailto:attaqiland@gmail.com"
                              class="text-gray-400 hover:text-yellow-500 hover:underline">attaqiland@gmail.com</a>
                        </li>
                        <li>
                           <a href="tel:081310202994" class="text-gray-400 hover:text-yellow-500 hover:underline">+62
                              813
                              1020 2994</a>
                        </li>
                        <li>
                           <a class="text-gray-400 hover:text-yellow-500 hover:underline">Setiap Hari (08.00 - 16.00
                              wib)</a>
                        </li>
                        <li>
                           <a href="https://goo.gl/maps/RKonCw6xYnKcr1TB6"
                              class="text-gray-400 hover:text-yellow-500 hover:underline">Jl. Nakula, Gentan-Purbayan,
                              Baki, Sukoharjo</a>
                        </li>
                     </nav>
                  </div>
                  <div class="lg:w-1/2 md:w-1/2 w-full px-4">
                     <!-- <h2 class="title-font font-medium text-white tracking-widest text-sm mb-3">CATEGORIES</h2> -->
                     <nav class="list-none mb-10">
                        <li>
                           <a href="/" class="text-gray-400 hover:text-yellow-500 hover:underline">About Us</a>
                        </li>
                        <li>
                           <a href="/progress" class="text-gray-400 hover:text-yellow-500 hover:underline">Progress</a>
                        </li>
                        <li>
                           <a href="/pricelist" class="text-gray-400 hover:text-yellow-500 hover:underline">Booklet &
                              Pricelist Request</a>
                        </li>
                     </nav>
                  </div>
               </div>
            </div>
         </div>
         <div class="flex flex-row pb-10">
            <div class="w-full flex md:items-center lg:items-start md:flex-row md:flex-nowrap flex-wrap flex-col">
               <div class="w-64 flex-shrink-0 md:mx-0 mx-auto text-center md:text-left">
                  <span class="inline-flex sm:ml-auto sm:mt-0 mt-2 justify-center sm:justify-start">
                     <a target="_blank" class="text-gray-400 mr-3  w-10 h-10 rounded-full text-center p-2"
                        href="https://www.facebook.com/Attaqi-Sharia-Townhouse-107068631007113/">
                        <svg fill="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                           class="w-5 h-5" viewBox="0 0 24 24">
                           <path d="M18 2h-3a5 5 0 00-5 5v3H7v4h3v8h4v-8h3l1-4h-4V7a1 1 0 011-1h3z"></path>
                        </svg>
                     </a>
                     <a target="_blank" class="text-gray-400 mr-3  w-10 h-10 rounded-full text-center p-2"
                        href="https://www.instagram.com/attaqi_land/" target="_blank" rel="nofollow">
                        <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round"
                           stroke-width="2" class="w-5 h-5" viewBox="0 0 24 24">
                           <rect width="20" height="20" x="2" y="2" rx="5" ry="5"></rect>
                           <path d="M16 11.37A4 4 0 1112.63 8 4 4 0 0116 11.37zm1.5-4.87h.01"></path>
                        </svg>
                     </a>
                  </span>
               </div>
               <div
                  class="flex-grow flex flex-wrap md:pl-20 -mb-10 md:mt-0 mt-10 md:text-left text-center justify-between">
                  <div class="lg:w-1/2 md:w-1/2 w-full px-4">
                     <p class="text-gray-400 text-sm text-center sm:text-left font-bold">Hunian Idaman Muslim</p>
                  </div>
                  <div class="lg:w-1/2 md:w-1/2 w-full px-4">
                     <p class="text-gray-400 text-sm text-center sm:text-left">© 2021 attaqiland.com —
                        <a target="_blank" href="https://www.instagram.com/attaqi_land/" target="_blank" rel="nofollow" rel="noopener noreferrer"
                           class="text-gray-500 ml-1" target="_blank">Attaqi Luxury Sharia Townhouse</a>
                     </p>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </footer>
</body>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.11.3/js/lightbox-plus-jquery.min.js" integrity="sha512-6gudNVbNM/cVsLUMOb8g2b/RBqtQJ3aDfRFgU+5paeaCTtbYY/Dg00MzZq7r6RvJGI2KKtPBhjkHGTL/iOe21A==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.min.js" defer></script>
<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
<script src="https://unpkg.com/swiper/swiper-bundle.js"></script>
<?php wp_footer();?>
<script>
   AOS.init();
</script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-DW1DEWEQ7Z"></script>
<script>
   window.dataLayer = window.dataLayer || [];
   function gtag() { dataLayer.push(arguments); }
   gtag('js', new Date());

   gtag('config', 'G-DW1DEWEQ7Z');
</script>

</html>
