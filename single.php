<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package attaqitheme
 */

// get_header();
?>

	<!-- <main id="primary" class="site-main"> -->

		<?php
// while (have_posts()):
//  the_post();

//  get_template_part('template-parts/content', get_post_type());

//  the_post_navigation(
//   array(
// 'prev_text' => '<span class="nav-subtitle">' . esc_html__('Previous:', 'attaqitheme') . '</span> <span class="nav-title">%title</span>',
// 'next_text' => '<span class="nav-subtitle">' . esc_html__('Next:', 'attaqitheme') . '</span> <span class="nav-title">%title</span>',
//   )
//  );

//  If comments are open or we have at least one comment, load up the comment template.
//  if (comments_open() || get_comments_number()):
//   comments_template();
//  endif;

// endwhile; // End of the loop.
?>

	<!-- </main>#main -->
	<!doctype html>
<html>

<head>
   <meta charset="UTF-8" />
   <meta name="viewport" content="width=device-width, initial-scale=1.0" />
   <link href="https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css" rel="stylesheet">
   	<link rel="stylesheet" href="https://attaqiland.com/wp-content/themes/attaqitheme/css/style.css">
   <style>

   </style>
</head>

<body>
   <header class="text-gray-400 bg-gray-900 body-font overflow-hidden">
      <div class="container mx-auto flex flex-wrap p-4 flex-col md:flex-row items-center">
         <a href="/" class="flex title-font font-medium items-center text-white mb-4 md:mb-0">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/LOGO+HD+(1).png" class="w-40 p-3" alt="">
         </a>
         <nav class="md:ml-auto flex flex-wrap items-center text-base justify-center">
            <a href="/" class="mr-5 hover:text-white">About Us</a>
            <a href="/progress" class="mr-5 hover:text-white">Progress</a>
         </nav>
         <a
            href="/pricelist" class="inline-flex items-center bg-yellow-600 border-0 py-1 px-3 focus:outline-none hover:bg-yellow-700 text-gray-900 md:mt-0 -mr-8 -pt-8 h-16 left-3 relative">Booklet & Pricelist Request
            <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
               class="w-4 h-4 ml-1" viewBox="0 0 24 24">
               <path d="M5 12h14M12 5l7 7-7 7"></path>
            </svg>
         </a>
      </div>
   </header>
   <section class="text-gray-600 bg-gray-200 body-font py-24">
   </section>
   <?php if (have_posts()) {
    the_post();
}?>

   <section class="text-gray-600 body-font pb-24 bg-gray-200">
      <div class="container px-5 mx-auto flex flex-col bg-white">
         <div class="lg:w-4/6 mx-auto">
            <div class="h-96 overflow-hidden relative bottom-32">
				<?php the_post_thumbnail('post-thumbnail', ['class' => 'object-cover object-center h-full w-full']);?>
            </div>
            <div class="sm:w-full px-4 relative bottom-12">
               <h2 class="title-font text-3xl font-medium text-gray-900 mt-6 mb-3"><?=the_title();?></h2>
            </div>
            <div class="flex flex-col sm:flex-row">
               <div class="sm:w-2/12 text-center sm:pr-8 sm:py-8">
                  <div class="w-20 h-20 rounded-full inline-flex items-center justify-center bg-gray-200 text-gray-400">
                     <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                        class="w-10 h-10" viewBox="0 0 24 24">
                        <path d="M20 21v-2a4 4 0 00-4-4H8a4 4 0 00-4 4v2"></path>
                        <circle cx="12" cy="7" r="4"></circle>
                     </svg>
                  </div>
                  <div class="flex flex-col items-center text-center justify-center">
                     <div class="w-12 h-1 bg-yellow-500 rounded mt-2 mb-4"></div>
                     <h2 class="font-medium title-font mt-4 text-gray-900 text-lg"><?php the_author();?></h2>
                     <h2 class="font-medium title-font mt-4 text-gray-900 text-lg"><?php ?></h2>
                     <!-- <p class="text-base">Raclette knausgaard hella meggs normcore williamsburg enamel pin sartorial venmo
                        tbh hot chicken gentrify portland.</p> -->
                  </div>
               </div>
               <div
                  class="sm:w-10/12 sm:pl-8 sm:py-8 sm:border-l border-gray-200 sm:border-t-0 border-t mt-4 pt-4 sm:mt-0 text-center sm:text-left">
                  <h3 class="tracking-widest text-yellow-500 text-xs font-medium title-font"><?=the_date()?></h3>
                  <p class="leading-relaxed text-lg mb-4"><?=the_content();?></p>
               </div>
            </div>
         </div>
      </div>
   </section>
   <footer class="text-gray-400 bg-gray-900 body-font overflow-hidden">
      <div class="bg-white md:ml-40 mt-20 px-10 ">
         <div class="flex flex-row">
            <div
               class="sm:pb-24 md:pb-12 pt-12 w-full flex md:items-center lg:items-start md:flex-row md:flex-nowrap flex-wrap flex-col">
               <div class="w-64 flex-shrink-0 md:mx-0 mx-auto text-center md:text-left">
                  <a href="/" class="flex title-font font-medium items-center md:justify-start justify-center text-white">
                     <img src="<?php echo get_template_directory_uri(); ?>/assets/img/LOGO+HD+(1).png" class="w-50 h-auto" alt="" srcset="">
                  </a>
                  <!-- <p class="mt-2 text-sm text-gray-500">Hunian Idaman Muslim</p> -->
               </div>
               <div
                  class="flex-grow flex w-full flex-wrap md:pl-20 -mb-10 md:mt-0 mt-10 md:text-left text-center justify-between">
                  <div class="lg:w-1/2 md:w-1/2  flex-grow w-full px-4">
                     <h2 class="title-font font-medium text-gray-800 tracking-widest text-lg mb-3">Kontak</h2>
                     <nav class="list-none mb-10">
                        <li>
                           <a href="mailto:attaqiland@gmail.com" class="text-gray-400 hover:text-yellow-500 hover:underline">attaqiland@gmail.com</a>
                        </li>
                        <li>
                           <a href="tel:081310202994" class="text-gray-400 hover:text-yellow-500 hover:underline">+62 813 1020 2994</a>
                        </li>
                        <li>
                           <a class="text-gray-400 hover:text-yellow-500 hover:underline">Setiap Hari (08.00 - 16.00
                              wib)</a>
                        </li>
                        <li>
                           <a href="https://goo.gl/maps/RKonCw6xYnKcr1TB6" class="text-gray-400 hover:text-yellow-500 hover:underline">Jl. Nakula, Gentan-Purbayan,
                              Baki, Sukoharjo</a>
                        </li>
                     </nav>
                  </div>
                  <div class="lg:w-1/2 md:w-1/2 w-full px-4">
                     <!-- <h2 class="title-font font-medium text-white tracking-widest text-sm mb-3">CATEGORIES</h2> -->
                     <nav class="list-none mb-10">
                        <li>
                           <a href="/" class="text-gray-400 hover:text-yellow-500 hover:underline">About Us</a>
                        </li>
                        <li>
                           <a href="/progress" class="text-gray-400 hover:text-yellow-500 hover:underline">Progress</a>
                        </li>
                        <li>
                           <a href="/pricelist" class="text-gray-400 hover:text-yellow-500 hover:underline">Booklet & Pricelist Request</a>
                        </li>
                     </nav>
                  </div>
               </div>
            </div>
         </div>
         <div class="flex flex-row pb-10">
            <div class="w-full flex md:items-center lg:items-start md:flex-row md:flex-nowrap flex-wrap flex-col">
               <div class="w-64 flex-shrink-0 md:mx-0 mx-auto text-center md:text-left">
                  <span class="inline-flex sm:ml-auto sm:mt-0 mt-2 justify-center sm:justify-start">
                     <a target="_blank" class="text-gray-400 mr-3  w-10 h-10 rounded-full text-center p-2" href="https://www.facebook.com/Attaqi-Sharia-Townhouse-107068631007113/">
                        <svg fill="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                           class="w-5 h-5" viewBox="0 0 24 24">
                           <path d="M18 2h-3a5 5 0 00-5 5v3H7v4h3v8h4v-8h3l1-4h-4V7a1 1 0 011-1h3z"></path>
                        </svg>
                     </a>
                     <a target="_blank" class="text-gray-400 mr-3  w-10 h-10 rounded-full text-center p-2" href="https://www.instagram.com/attaqi_land/">
                        <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round"
                           stroke-width="2" class="w-5 h-5" viewBox="0 0 24 24">
                           <rect width="20" height="20" x="2" y="2" rx="5" ry="5"></rect>
                           <path d="M16 11.37A4 4 0 1112.63 8 4 4 0 0116 11.37zm1.5-4.87h.01"></path>
                        </svg>
                     </a>
                  </span>
               </div>
               <div
                  class="flex-grow flex flex-wrap md:pl-20 -mb-10 md:mt-0 mt-10 md:text-left text-center justify-between">
                  <div class="lg:w-1/2 md:w-1/2 w-full px-4">
                     <p class="text-gray-400 text-sm text-center sm:text-left font-bold">Hunian Idaman Muslim</p>
                  </div>
                  <div class="lg:w-1/2 md:w-1/2 w-full px-4">
                     <p class="text-gray-400 text-sm text-center sm:text-left">© 2021 attaqiland.com —
                        <a target="_blank" href="https://www.instagram.com/attaqi_land/" rel="noopener noreferrer"
                           class="text-gray-500 ml-1" target="_blank">Attaqi Luxury Sharia Townhouse</a>
                     </p>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </footer>
</body>

</html>

<?php
// get_sidebar();
// get_footer();
